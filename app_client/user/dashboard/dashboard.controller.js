/**
 * Created by wendell on 11/5/17.
 */

(function () {
    'use strict';

    function DashboardController ($scope, userServices, eventsService, authentication, $location, globalModalService, $uibModal, logger, $timeout) {
        const vm = this;
        $scope.switchArchived = false ;
        $scope.currentUser = authentication.getCurrentUser();
        $scope.isCollapsed = true;
        $scope.signupIsCollapsed = true;

        $scope.$on('$newEventCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$newEventAdded', function () { $scope.isCollapsed = true; vm.updateDashboard(); });

        /** Adding */
        vm.eventAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {
                angular.element('#input-2').focus(); // TODO: será que será sempre esse nome?
            }, 100);

        };

        vm.checkFinished = function () {
            const today = new Date();
            today.setDate(today.getDate()-1);
            for (const idx in vm.dashboardData.events) {
                vm.dashboardData.events[idx].finished = new Date(vm.dashboardData.events[idx].date) < today;
            }
        };

        function checkTasks() {
            const tasks = [];
            vm.dashboardData.events.forEach(function (event) {
                event.tasks.forEach(function (task) {
                    task.eventName = event.type + ' de ' + event.mainCustomers;
                    task.eventId = event._id;
                    if (!task.done) {
                        tasks.push(task);
                    }
                })
            });
            tasks.sort( function(a, b) {
                return new Date(a.dueDate) - new Date(b.dueDate);
            });
            vm.dashboardTasks = tasks;
        }

        vm.updateDashboard = function () {
            userServices
                .dashboard()
                .then(function(data) {
                    vm.dashboardData = { events : data };
                    vm.checkFinished();
                    checkTasks();
                }, function(e) {
                    /**
                     * Direciona usuários não autorizados para tela de login
                     * TODO: forçar resfresh para evitar back e usar app.
                     * */
                    if( e.data.status === 401 || e.data.message.includes('UnauthorizedError') ) {
                        authentication.logoutAndClose();
                        $location.path('/');
                    }
                    console.error(e);
                });
        };
        vm.updateDashboard();

        $scope.goToTask = function (eventId) {
            const index = vm.dashboardData.events.findIndex( function (a) { return a._id === eventId; } );
            eventsService.setActiveEvent(vm.dashboardData.events[index]);
            $location.path('/tasks');
        };

        $scope.eventClick = function (event) {
            eventsService.setActiveEvent(event);
            $location.path('/customers');
        };

        vm.creatEvent = function () {
            let trial;
            eventsService
                .getQtdyUserEvents()
                .then(function (qtdy) {
                    trial = authentication.getCurrentUser().trial;
                    if(trial && qtdy >= 2) {
                        logger.logWarning('Durante o período de teste você consegue criar no máximo 2 Eventos.');
                        $scope.signupIsCollapsed = !$scope.signupIsCollapsed;
                    } else {
                        $scope.isCollapsed = !$scope.isCollapsed;
                    }
                })
                .catch(function (err) {
                    console.error(err);
                })
        };

        /** Delete */
        vm.deleteEvent = function (eventId) {
            const modalInstance = $uibModal.open( globalModalService.getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Apagar Evento', 'Tem certeza que deseja apagar esse Evento?', 'ATENÇÃO: Isso não poderá ser desfeito!' ) );
            modalInstance.result.then(function () {
                eventsService
                    .deleteEvent( eventId )
                    .then( function () {
                        vm.updateDashboard();
                        logger.logSuccess('Evento apagado com sucesso!');
                    }, function(e) {
                        console.error(e);
                        logger.logError('Erro ao apagar evento!');
                    });
            });
        };
    }

    angular
        .module('app.user')
        .controller('DashboardController', ['$scope', 'userServices', 'eventsService', 'authentication', '$location', 'globalModalService', '$uibModal', 'logger', '$timeout', DashboardController]);

})();