/**
 * Created by wendell on 1/10/18.
 */

(function () {
    'use strict';

    function ProfileController(logger, $uibModal, $scope, authentication, userServices, eventsService, globalModalService) {
        const vm = this;
        vm.currentUser = authentication.getCurrentUser();

        vm.canSubmit = function () {
            return $scope.formEditCurrentUser.$valid;
        };
        vm.submitForm = function () {
            authentication
                .userUpdate(vm.currentUser)
                .then(function () {
                    logger.logSuccess('Perfil atualizado com sucesso!');
                })
                .catch(function (err) {
                    logger.logError('Erro ao atualizar o perfil! ' + err.data.message);
                    console.error(err);
                });
        };


        /** Reactive */
        vm.reactiveEvent = function (event) {
            const modalInstance = $uibModal.open(globalModalService.getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Reaticonst Evento', 'Tem certeza que deseja areaticonst esse Evento?'));
            modalInstance.result.then(function () {
                eventsService
                    .archiveEvent(event._id, {archive: false})
                    .then(function () {
                        const index = vm.archivedEvents.findIndex(function (x) {
                            return x._id === event._id;
                        });
                        vm.archivedEvents.splice(index, 1);
                        logger.logSuccess('Reativado com sucesso!');
                    }, function (e) {
                        console.error(e);
                        logger.logError('Erro ao reaticonst evento!');
                    });
            });
        };
    }

    angular
        .module('app.user')
        .controller('ProfileController', ['logger', '$uibModal', '$scope', 'authentication', 'userServices', 'eventsService', 'globalModalService', ProfileController]);

})();
