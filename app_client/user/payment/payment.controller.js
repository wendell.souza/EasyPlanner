/**
 * Created by wendell on 1/10/18.
 */
(function () {
    'use strict';

    function PaymentController(authentication, userServices, logger, paymentModalService) {
        const vm = this;
        vm.currentUser = authentication.getCurrentUser();

        vm.subscription = () => {
            paymentModalService
                .paymentModal('Assinatura Easyplanner')
                .then(subscription => {
                    logger.logSuccess('Assinatura realizada com sucesso!');
                    vm.currentUser = authentication.getCurrentUser();
                    getPaymentCustomer();
                    getPaymentHistory();
                })
                .catch(err => {
                    logger.logError('Erro ao finalizar assinatura.');
                    console.error(err);
                })
        };
        vm.updateCard = () => {
            paymentModalService
                .paymentModal('Alterar Cartão')
                .then(subscription => {
                    logger.logSuccess('Cartão alterado com sucesso.');
                    getPaymentCustomer();
                })
                .catch(err => {
                    // logger.logError('Erro ao alterar o cartão.');
                    console.error(err);
                })
        };

        const getPaymentHistory = () => {
            userServices
                .getPaymentHistory()
                .then( history => {
                    vm.paymentHistory = history.data.data ;
                })
                .catch(err => {
                   console.log('err: ', err);
                });
        };
        if(!vm.currentUser.trial) {
            getPaymentHistory();
        }

        const getPaymentCustomer = () => {
            userServices
                .getPaymentCustomer()
                .then( stripeCustomer => {
                    vm.paymentCustomer  = stripeCustomer.data;
                    vm.currentUser.plan = vm.paymentCustomer.subscriptions.data[0].plan.nickname;
                })
                .catch(err => {
                   console.log(err);
                   logger.logError('Erro ao consultar o Plano atual.');
                });
        };
        if(!vm.currentUser.trial) {
            getPaymentCustomer();
        } else {
            vm.currentUser.plan = 'Período de Teste';
        }
    }

    angular
        .module('app.user')
        .controller('PaymentController', ['authentication', 'userServices', 'logger', 'paymentModalService', PaymentController]);

})();