/**
 * Created by wendell on 8/27/18.
 */

(function () {
    'use strict';

    function paymentModalService ($mdDialog) {

        const paymentModal = function(title) {

            return new Promise (function (resolve, reject) {
                $mdDialog.show({
                    controller: 'PaymentModalController',
                    templateUrl: '/user/payment/paymentModal/paymentModal.html',
                    parent: angular.element(document.body),
                    controllerAs: 'vm',
                    fullscreen: false, // Only for -xs, -sm breakpoints.
                    clickOutsideToClose: true,
                    resolve: {
                        title: function () { return title; }
                    }
                }).then(function(answer) {
                    resolve(answer);
                }, function(error) {
                    reject(error);
                });
            })
        };

        return {
            paymentModal : paymentModal
        }
    }

    angular
        .module('app.user')
        .service('paymentModalService', [ '$mdDialog', paymentModalService])
})();