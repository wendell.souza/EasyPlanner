(function () {
    'use strict';
    
    angular
        .module('app', [
            //** Angular modules */
             'ngAnimate'
            ,'ngAria'
            ,'ngMessages'
            
            //** 3rd Party Modules */
            ,'ngMaterial'
            ,'ui.router'
            ,'ui.router.state.events'
            ,'ui.bootstrap'
            ,'angular-loading-bar'
            ,'duScroll'
            ,'idf.br-filters'
            ,'ui.utils.masks'
            ,'ngFileUpload'

            //** easyPlanner */
            ,'app.authentication'
            ,'app.user'
            ,'app.mainScreen'
            ,'app.events'
            ,'app.i18n'
            ,'app.nav'
        ]);
})();








