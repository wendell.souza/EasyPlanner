/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';
    
    function OkCancelModalCtrl($scope, $uibModalInstance, title, message, secondMessage) {
        $scope.title         = title;
        $scope.message       = message;
        $scope.secondMessage = secondMessage;

        $scope.ok = function() {
            $uibModalInstance.close();
        };
        
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function globalModalService () {
        this.getUibModalObj = function (tamplateUrl, controller, title, message, secondMessage) {
            return {
                templateUrl: tamplateUrl,
                controller: controller,
                resolve: {
                    title: function () { return title; },
                    message: function () { return message; },
                    secondMessage: function () { return secondMessage; }
                }
            };
        };
    }
    
    angular
        .module('app')
        .controller('OkCancelModalCtrl', ['$scope', '$uibModalInstance', 'title', 'message', 'secondMessage', OkCancelModalCtrl])
        .service('globalModalService', [globalModalService]);
    
})();