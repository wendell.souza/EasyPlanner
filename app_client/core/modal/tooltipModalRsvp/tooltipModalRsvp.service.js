/**
 * Created by wendell on 10/26/18.
 */

(function () {
    'use strict';

    function tooltipModalRsvpService ($mdDialog) {

        const tooltipModal = function() {

            return new Promise (function (resolve, reject) {
                $mdDialog.show({
                    controller: 'tooltipModalRsvpController',
                    templateUrl: 'core/modal/tooltipModalRsvp/tooltipModalRsvp.html',
                    parent: angular.element(document.body),
                    // targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: false // Only for -xs, -sm breakpoints.
                    // resolve: {}
                }).then(function(answer) {
                    resolve(answer);
                }, function(error) {
                    reject(error);
                });
            })
        };

        return {
            tooltipModal : tooltipModal
        }
    }

    angular
        .module('app')
        .service('tooltipModalRsvpService', [ '$mdDialog', tooltipModalRsvpService])
})();