/**
 * Created by wendell on 10/24/18.
 */
(function () {
    'use strict';

    function tooltipModalRsvpController($scope, $mdDialog) {
        $scope.close = () => {
            $mdDialog.hide();
        };

        $scope.next = () => {
            $scope.selectedIndex = $scope.selectedIndex === 3 ? 0 : $scope.selectedIndex+1;
        };

        $scope.prior = () => {
            $scope.selectedIndex = $scope.selectedIndex === 0 ? 3 : $scope.selectedIndex -1;
        };
    }

    angular
        .module('app')
        .controller('tooltipModalRsvpController', ['$scope', '$mdDialog', tooltipModalRsvpController]);
})();