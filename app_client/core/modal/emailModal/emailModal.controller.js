/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';

    function EmailModalController($scope, $mdDialog, eventsService, title, subject, mailTo, mailText, appService, logger, html2Pdf, authentication, providerService, customerService) {
        $scope.title                 = title;
        $scope.subject               = subject;
        $scope.mailText              = mailText;
        $scope.mailToList            = mailTo;
        $scope.html2Pdf              = html2Pdf;
        $scope.isCollapsed           = true;
        $scope.isMailToEditCollapsed = true;
        $scope.customers             = customerService.getCustomerEmailList();
        $scope.selectedCustomers     = [];
        $scope.vendors               = providerService.getVendorsEmailList();
        $scope.selectedVendors       = [];

        const form = document.getElementsByName('formSendMail');
        console.log( form );

        let activeEvent, mailFrom;
        activeEvent = eventsService.getActiveEvent();
        activeEvent = activeEvent.type + ' de ' + activeEvent.mainCustomers;

        mailFrom = authentication.getCurrentUser();
        mailFrom = mailFrom.email;

        $scope.toggleCustomers = function (item, list) {
            let idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.existsCustomers = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminateCustomers = function() {
            return ($scope.selectedCustomers.length !== 0 &&
                $scope.selectedCustomers.length !== $scope.customers.length);
        };

        $scope.isCheckedCustomers = function() {
            return $scope.selectedCustomers.length === $scope.customers.length;
        };

        $scope.toggleAllCustomers = function() {
            if ($scope.selectedCustomers.length === $scope.customers.length) {
                $scope.selectedCustomers = [];
            } else if ($scope.selectedCustomers.length === 0 || $scope.selectedCustomers.length > 0) {
                $scope.selectedCustomers = $scope.customers.slice(0);
            }
        };

        $scope.toggleVendors = function (item, list) {
            let idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.existsVendors = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminateVendors = function() {
            return ($scope.selectedVendors.length !== 0 &&
                $scope.selectedVendors.length !== $scope.vendors.length);
        };

        $scope.isCheckedVendors = function() {
            return $scope.selectedVendors.length === $scope.vendors.length;
        };

        $scope.toggleAllVendors = function() {
            if ($scope.selectedVendors.length === $scope.vendors.length) {
                $scope.selectedVendors = [];
            } else if ($scope.selectedVendors.length === 0 || $scope.selectedVendors.length > 0) {
                $scope.selectedVendors = $scope.vendors.slice(0);
            }
        };

        $scope.send = function() {

            if( !$scope.subject ){
                logger.logWarning('Assunto precisa ser informado!');
                return;
            }
            if( !$scope.mailText ) {
                logger.logWarning('Corpo do e-mail precisa ser informado!');
                return;
            }

            let input = $scope.mailToList.split(',');
            let mailTo = [];
            input.forEach(function (item) {
                item = {'email' : item};
                mailTo.push(item);
            });

            let mailBcc = $scope.selectedVendors.concat($scope.selectedCustomers);

            let emailObj = {
                title      : $scope.title,
                subject    : $scope.subject,
                mailFrom   : mailFrom,
                mailTo     : mailTo,
                mailBcc    : mailBcc,
                mailText   : $scope.mailText,
                event      : activeEvent,
                html2Pdf   : $scope.html2Pdf
            };

            appService
                .sendGeneralEmail(emailObj)
                .then(function(){
                    $mdDialog.hide('E-mail envidado com sucesso.');
                })
                .catch(function (err) {
                    logger.logError('Erro ao enviar e-mail. ' + err);
                    $mdDialog.cancel(err);
                })
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }

    angular
        .module('app')
        .controller('EmailModalController', ['$scope', '$mdDialog', 'eventsService','title', 'subject', 'mailTo', 'mailText', 'appService', 'logger', 'html2Pdf', 'authentication', 'providerService', 'customerService', EmailModalController]);
})();