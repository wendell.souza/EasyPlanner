(function () {
    'use strict';

    function logger() {

        var logIt;

        // toastr setting.
        toastr.options = {
            "closeButton": true,
            "positionClass": "toast-top-right",
            "timeOut": "4000",
            "preventDuplicates": true,
            "newestOnTop": true,
            "progressBar": true
        };

        logIt = function(message, type) {
            return toastr[type](message);
        };

        return {
            log: function(message) {
                logIt(message, 'info');
            },
            logWarning: function(message) {
                logIt(message, 'warning');
            },
            logSuccess: function(message) {
                logIt(message, 'success');
            },
            logError: function(message) {
                logIt(message, 'error');
            }
        };

    }

    angular
        .module('app')
        .factory('logger', logger);

})();