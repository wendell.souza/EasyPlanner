/**
 * Created by wendell on 3/13/17.
 */
( function() {
    angular
        .module('app')
        .service('appService', [ '$http', 'authentication', appService]);
    
    function appService ($http, authentication) {
        /** TODO: dividir esse serviços em tipos */
        /** C O N S T A N T S  */
        const ACTION_UPDATE         = 'ACTION_UPDATE';
        const ACTION_DELETE         = 'ACTION_DELETE';
        const FORM_INCOMPLETE_ERROR = 'Informe todos os campos e tente novamente.';
        var authenticationObject = {
            headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }
        };
        String.prototype.paddingLeft = function (paddingValue) {
            return String(paddingValue + this).slice(-paddingValue.length);
        };
        var arrayObjectIndexOf = function (key, prop, array) {
            for (var i=0; i < array.length; i++) {
                if (array[i][prop] === key) {
                    return i;
                }
            }
            return -1;
        };
        var getFormattedTime = function (date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            hours = hours.toString().paddingLeft("00");
            minutes = minutes.toString().paddingLeft("00");
            return hours + ":" + minutes;
        };
        var apiLog = function (log) {
            return $http.post('/api/log', log, authenticationObject);
        };
        var getUrlServer = function () { //TODO: mudar o nome para ChatServer? pois fica estranho configurar uma variável para o servidor da aplicação
            return $http.get('/api/server/url', authenticationObject);
        };
        var daysUntil = function (date) {
            var dateEvent, dateNow, oneDay;
            dateEvent = new Date(date);
            dateNow   = new Date(Date.now());
            oneDay    = 1000 * 60 * 60 * 24;
            return Math.round(( dateEvent.getTime() - dateNow.getTime() ) / (oneDay));
        };
        var sendGeneralEmail  = function(emailObj) {
            return new Promise(function(resolve, reject) {
                $http
                    .post('/api/sendemail/', emailObj, authenticationObject)
                    .then(function () {
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            })
        };
        return {
            arrayObjectIndexOf : arrayObjectIndexOf,
            getFormattedTime : getFormattedTime,
            apiLog : apiLog,
            getUrlServer : getUrlServer,
            daysUntil : daysUntil,
            sendGeneralEmail : sendGeneralEmail,
            ACTION_UPDATE : ACTION_UPDATE,
            ACTION_DELETE : ACTION_DELETE,
            FORM_INCOMPLETE_ERROR : FORM_INCOMPLETE_ERROR
        };
    }
}) ();