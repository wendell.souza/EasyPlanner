/**
 * Created by wendell on 6/26/18.
 */
( function () {
    'use strict';

    function ScriptsController($scope, $location, $uibModal, $timeout, eventsService, scriptService, logger,
                               globalModalService, $rootScope) {

        $scope.$on('$editScriptCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editScriptAdded', function () { $scope.isCollapsed = true;  });
        $scope.$on('$activeEventUpdated', function () { vm.updateScripts(); });

        var vm = this;
        vm.eventInfo = eventsService.getActiveEvent();

        vm.sortScripts = function () {
            if(!vm.eventInfo.scripts) { return; }
            vm.eventInfo.scripts.sort( function(a, b) {
                return a.hour ? a.hour.localeCompare(b.hour) : -1;
            });
        };

        vm.editScript = scriptService.getScript();

        vm.updateScripts = function() {
            vm.eventInfo = eventsService.getActiveEvent();
            vm.sortScripts();
        };

        vm.updateScripts();

        /** Adding */
        vm.scriptAdd = function () {
            $location.path('/episodes');
            $timeout(function () {
                $rootScope.$emit('$toAddScript');
            }, 500)
        };
        /** Update */
        vm.scriptEdit = function (script, index) {
            scriptService.setScript(script);
            $location.path('/episodes');
        };

        /** Delete */
        vm.scriptDelete = function (script) {
            var modalInstance = $uibModal
                .open(
                    globalModalService
                        .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                            'OkCancelModalCtrl',
                            'Excluir Roteiro',
                            'Confirma a exclusão deste Roteiro?' )
                );
            modalInstance.result.then(function () {
                scriptService.deleteEventScript(script)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        logger.logSuccess('Roteiro apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Roteiro.');
                        console.error(err);
                    });
            });
        };
    }

    function EditScriptController($scope, $location, scriptService, eventsService, logger, $timeout, $rootScope) {

        var vm = this;
        $scope.isEditScriptCollapsed = true;

        $rootScope.$on('$toAddScript', function () {
            vm.editScript = { 'name' : 'Novo Script' };
            $timeout(function () {
                    $scope.isEditScriptCollapsed = false;
                }, 200
            );
            $timeout(function () {
                    angular.element('#editScriptName').focus();
                }, 500
            );
        });

        vm.editScript = scriptService.getScript();
        vm.cleanObj = function () {
            scriptService.clearScript();
            vm.editScript = {};
        };
        vm.canSubmit = function() {
            return $scope.formEditScript.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editScriptCancelled');
        };
        vm.submitForm = function () {
            vm.addScript(vm.editScript);
        };
        vm.addScript = function(script) {
            scriptService
                .editEventScript(script)
                .then(function (script) {
                    $scope.$emit('$editScriptAdded');
                    logger.logSuccess('Roteiro Incluído/atualizado com sucesso.');
                    vm.editScript = script;
                })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Roteiro.');
                    console.error(err);
                });
        };
    }

    angular
        .module('app.scripts')
        .controller('ScriptsController', ['$scope','$location', '$uibModal', '$timeout', 'eventsService',
            'scriptService', 'logger', 'globalModalService', '$rootScope', ScriptsController])
        .controller('EditScriptController', [ '$scope', '$location', 'scriptService', 'eventsService',
            'logger', '$timeout', '$rootScope', EditScriptController]);
}) ();