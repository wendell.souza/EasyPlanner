/**
 * Created by wendell on 1/29/18.
 */
( function () {
    'use strict';

    function EpisodesController($scope, $location,     $uibModal, $timeout, eventsService, episodeService, logger, globalModalService, scriptService, $rootScope, emailModalService, $window) {
        $scope.$on('$editEpisodeCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editEpisodeAdded', function () {
            $timeout( function () {
                $scope.isCollapsed = true;
                vm.updateEpisodes();
            }, 1000);
        });
        $scope.$on('$activeEventUpdated', function () { vm.updateEpisodes(); });
        $rootScope.$on('$toAddScript', function () { $scope.adding = true; });
        $scope.$on('$editScriptAdded', function () { $scope.adding = false; });

        let vm = this;
        vm.eventInfo = eventsService.getActiveEvent();
        vm.editScript = scriptService.getScript();

        vm.sortEpisodes = function () {
            if(!vm.editScript.episodes) { return; }
            vm.editScript.episodes.sort( function(a, b) {
                return a.hour ? a.hour.localeCompare(b.hour) : -1;
            });
        };
        vm.updateEpisodes = function() {
            vm.editScript = scriptService.getScript();
            vm.sortEpisodes();
        };

        vm.updateEpisodes();
        $scope.isCollapsed = true;

        vm.backToScripts = function() {
            $location.path('/scripts');
            scriptService.clearScript();
        };
        /** Adding */
        vm.episodeAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#episodeHour').focus(); }, 100);
        };
        /** Update */
        vm.episodeEdit = function (episode) {
            episodeService.setEpisode(episode);
            $location.path('/updateEpisode');
            $timeout(function () {angular.element('#episodeHour').focus(); }, 100);
        };

        /** Delete */
        vm.episodeDelete = function (episode) {
            episode.scriptId = vm.editScript._id;
            let modalInstance = $uibModal.open(
                globalModalService
                    .getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Excluir Episódio', 'Confirma a exclusão deste Episódio?' )
            );
            modalInstance.result.then(function () {
                episodeService
                    .deleteEventEpisode(episode)
                    .then( function() {
                        vm.updateEpisodes();
                        logger.logSuccess('Episódio apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Episódio.');
                        console.error(err);
                    });
            });
        };

        /**
         * Imprimir na mesma janela
         */
        vm.printHere = function() {
            window.print();
        };

        /** Imprimir em outra janela */
        vm.printWindow =  function () {
            let printContents, popupWin;

            printContents = document.getElementById('roteiro').innerHTML;
            popupWin = $window.open();
            popupWin.document.open();
            popupWin.document.write('' +
                '<html>' +
                '<head>' +
                '<link rel="stylesheet" type="text/css"  href="../../styles/main.css" />' +
                '<link rel="stylesheet" type="text/css"  href="../../styles/bootstrap.css">' +
                '<link rel="stylesheet" type="text/css"  href="../../styles/ui.css">' +
                '</head>' +
                '<body onload="window.print()">'
                + printContents +
                '</body>' +
                '</html>');
            popupWin.document.close();

        };

        /** Send E-mail */
        vm.sendEmail =  function () {
            let scriptHtml = document.getElementById('roteiro').innerHTML;

            emailModalService
                .emailModal(
                    'Compartilhar Roteiro' ,
                    '',
                    '',
                    '',
                    scriptHtml
                )
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        logger.logError('Erro ao enviar e-mail.');
                        console.error(error);
                    }
                });
        }
    }


    function EditEpisodeController($scope, $location, episodeService, eventsService, logger, scriptService) {
        let vm = this;
        vm.back = function () { $location.path('/episodes'); };
        vm.editEpisode = episodeService.getEpisode();
        if (angular.equals(vm.editEpisode, {}) ) { vm.back(); } /** Se não houver episode em memória volta pra lista */
        vm.script = scriptService.getScript();
        vm.cleanObj = function () {
            episodeService.clearEpisode();
            vm.editEpisode = {};
        };
        vm.canSubmit = function() {
            return $scope.formEditEpisode.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editEpisodeCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            vm.editEpisode.scriptId = vm.script._id;
            vm.editEpisode.scriptIndex = vm.script.index;
            episodeService
                .editEventEpisode(vm.editEpisode)
                .then( function() {
                    vm.cleanObj();
                    $scope.$emit('$editEpisodeAdded');
                    logger.logSuccess('Episódio Incluído/atualizado com sucesso.');
                    vm.back();
                })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Episódio.');
                    console.error(err);
                });
        };
    }

    angular
        .module('app.episodes')
        .controller('EpisodesController', ['$scope','$location', '$uibModal', '$timeout', 'eventsService', 'episodeService', 'logger', 'globalModalService', 'scriptService', '$rootScope', 'emailModalService', '$window', EpisodesController])
        .controller('EditEpisodeController', [ '$scope', '$location', 'episodeService', 'eventsService', 'logger', 'scriptService', EditEpisodeController]);
}) ();