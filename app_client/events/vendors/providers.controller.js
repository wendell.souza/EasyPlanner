/**
 * Created by wendell on 30/01/18.
 */

( function () {  // wrap in IIFE (Immediately Invoked Function Expression)
    'use strict';
    function ProvidersController ($scope, $uibModal, $log, $location, $http, $timeout, eventsService, providerService,
                                  logger, globalModalService, $mdDialog, authentication, emailModalService) {
        var vm = this;
        $scope.isCollapsed = true;
        $scope.switchContracted = true;

        function updateAll() {
            vm.eventInfo = eventsService.getActiveEvent();
            if (vm.eventInfo) {
                // updateGraph();
                vm.eventInfo.contractedProviders = [];
                vm.eventInfo.nonContractedProviders = [];
                checkContracted();
            }
        }

        updateAll();

        function sortByType(array) {
            array.sort(function (a, b) {
                if(a.type && b.type) { return a.type.localeCompare(b.type); }
                else if (!a.type) { return 1; }
                else if (!b.type) { return -1; }
            })
        }
        function checkContracted() {
            vm.eventInfo.providers.forEach(function (provider) {
                if ( !provider.contracted ) {
                    provider.contracted = false;
                    vm.eventInfo.nonContractedProviders.push(provider);
                    sortByType(vm.eventInfo.nonContractedProviders);
                } else {
                    vm.eventInfo.contractedProviders.push(provider);
                    sortByType(vm.eventInfo.contractedProviders);
                }
            });
        }

        $scope.$on('$editProviderCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editProviderAdded', function () { $scope.isCollapsed = true; updateAll();  updateGraph(); });
        $scope.$on('$activeEventUpdated', function () { updateAll(); });

        /** Adding */
        vm.providerAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#providerName').focus(); }, 100);
        };
        /** Update */
        vm.providerEdit = function (provider) {
            providerService.setProvider(provider);
            $location.path('/updateVendor');
            $timeout(function () {angular.element('#providerName').focus(); }, 100);
        };

        /** Delete */
        vm.providerDelete = function (provider) {
            var modalInstance = $uibModal
                .open( globalModalService
                    .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                        'OkCancelModalCtrl',
                        'Excluir Fornecedor',
                        'Confirma a exclusão deste Fornecedor?' )
                );
            modalInstance.result.then(function () {
                eventsService.deleteEventProvider(provider)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        updateAll();
                        // updateGraph();
                        logger.logSuccess('Fornecedor apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Fornecedor.');
                        console.error(err);
                    });
            });
        };

        /** Attachements */
        vm.uploadFile = function(provider) {
            if (!vm.fileUpload) { return; }
            eventsService
                .uploadProviderFile(provider, vm.fileUpload)
                .then(function () {
                    updateAll();
                    logger.logSuccess('Upload feito com sucesso.');
                })
                .catch(function (err) {
                    console.error(err);
                    logger.logError('Erro ao fazer o Upload.' + err.message);
                });
        };
        vm.DownloadFile = function(provider, file) {
            eventsService
                .downloadProviderFile(provider, file)
                .then(function(){
                    logger.logSuccess('Arquivo baixado com sucesso.');
                })
                .catch(function (err) {
                    console.error(err);
                    logger.logError('Erro ao baixar arquivo.');
                });
        };
        vm.DeleteFile = function(provider, file) {
            var modalInstance = $uibModal.open( globalModalService
                .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                    'OkCancelModalCtrl',
                    'Apagar Arquivo Anexo',
                    'Confirma a exclusão deste Arquivo?  ' + file.fileName )
            );
            modalInstance.result.then(function () {
                eventsService
                    .deleteProviderFile(provider, file)
                    .then(function () {
                        updateAll();
                        logger.logSuccess('Arquivo apagado com sucesso.');
                    })
                    .catch(function (err) {
                        console.error(err);
                        logger.logError('Erro ao apagar arquivo.');
                    });
            });
        };
        /** Send E-mail */
        vm.sendEmail =  function (provider) {
            if (!provider.email) {
                logger.logWarning('Cadastre o e-mail para compartilhar algo com seu Fornecedor.');
                return false;
            }
            emailModalService
                .emailModal(
                    'E-mail para ' + provider.name,
                    '',
                    provider.email,
                    ''
                )
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        logger.logError('Erro ao enviar e-mail.');
                        console.error(error);
                    }
                });
        }
    }

    function EditProviderController($scope, $location, $rootScope, providerService, eventsService, logger) {
        var vm = this;
        vm.back = function () { $location.path('/vendors'); };
        vm.editProvider = providerService.getProvider();

        if (angular.equals(vm.editProvider, {}) ) { vm.back(); } /** Se não houver provider em memória volta pra lista */

        vm.cleanObj = function () {
            providerService.clearProvider();
            vm.editProvider = {};
            vm.selectedItem = null;
            vm.searchText = null;
        };
        vm.canSubmit = function() {
            return $scope.formEditProvider.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editProviderCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            eventsService.editEventProvider(vm.editProvider)
                .then( function() {
                    vm.cleanObj();
                    $scope.$emit('$editProviderAdded');
                    $rootScope.$broadcast('$activeEventUpdatedLocally');
                    logger.logSuccess('Fornecedor Incluído/atualizado com sucesso.');
                    vm.back(); })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Fornecedor.');
                    console.error(err);
                });
        };
        /** Variáveis para md-autocomplete */
        vm.types = eventsService.providersTypes;
        vm.selectedItem = vm.editProvider.type;
        vm.simulateQuery = false;
        vm.isDisabled = false;
        vm.noCache = true;
        /** Internal methods md-autocomplete */
        function createFilterFor(query) {
            return function filterFn(type) {
                return (angular.lowercase(type).indexOf(angular.lowercase(query)) > -1);
            };
        }
        function querySearch (query) {
            return query ? vm.types.filter( createFilterFor(query) ) : vm.types;
        }
        function searchTextChange(text) {
            vm.editProvider.type = text;
        }
        function selectedItemChange(item) {
            vm.editProvider.type = item;
        }
        vm.querySearch   = querySearch;
        vm.selectedItemChange = selectedItemChange;
        vm.searchTextChange   = searchTextChange;

    }

    angular
        .module('app.providers')
        .controller('ProvidersController', ['$scope', '$uibModal', '$log', '$location', '$http', '$timeout', 'eventsService', 'providerService', 'logger', 'globalModalService', '$mdDialog', 'authentication', 'emailModalService', ProvidersController])
        .controller('EditProviderController', [ '$scope', '$location', '$rootScope', 'providerService', 'eventsService', 'logger', EditProviderController]);
}) ();
