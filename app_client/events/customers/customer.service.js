/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';
    
    function customerService ($http, eventsService, authentication) {
        let _customer = {};
        
        this.setCustomer = function (customer) {
            _customer = customer;
        };
        
        this.getCustomer = function () {
            return _customer;
        };
        
        this.clearCustomer = function  () {
            this.setCustomer({});
        };

        this.CUSTOMERS_ROLES = ['Noiva', 'Noivo', 'Aniversariante', 'Cliente'];

        this.editEventCustomer = function (customer) {
            return new Promise( function (resolve, reject) {
                let event = eventsService.getActiveEvent();
                let url = customer._id ?
                    '/api/events/' + event._id + '/customers/' + customer._id :     /** Update */
                    '/api/events/' + event._id + '/customers/';                     /** Add */
                $http
                    .post(url, customer, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newCustomer) {
                        eventsService.eventAddObjById(event.customers, newCustomer.data);
                        eventsService.setActiveEvent(event);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        this.deleteEventCustomer = function (customer) {
            return new Promise( function(resolve, reject) {
                let event = eventsService.getActiveEvent();
                $http
                    .delete('/api/events/' + event._id + '/customers/' + customer._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventsService.eventDeleteObjById(event.customers, customer);
                        eventsService.setActiveEvent(event);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        this.inviteEventCustomers = function () {
            return new Promise( function(resolve, reject) {
                let event = eventsService.getActiveEvent();
                $http({
                    method: 'POST',
                    data: null,
                    url: '/api/events/' + event._id + '/customers/invite',
                    headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }
                }).then(function () {
                    resolve();
                }).catch(function (err) {
                    reject(err.data);
                });
            });
        };
        this.getCustomerEmailList = function () {
            let eventInfo = eventsService.getActiveEvent(),
                emailList = [];
            eventInfo.customers.forEach(function (customer) {
                if (customer.email) {
                    emailList.push({
                        'name'  : customer.name,
                        'email' : customer.email });
                }
            });
            return emailList;
        };
    }
    
    angular
        .module('app.customers')
        .service('customerService', ['$http', 'eventsService', 'authentication', customerService]);
    
})();