/**
 * Created by wendell on 12/5/17.
 */
(function () {
    'use strict';
    function CustomersController ($scope, $uibModal, $log, $location, $timeout, eventsService, customerService, logger,
                                  globalModalService, authentication, emailModalService) {
        var vm = this;
        vm.eventInfo = eventsService.getActiveEvent();
        if (!vm.eventInfo) { $location.path('/dashboard'); }
        vm.currentUser = authentication.getCurrentUser();
        vm.roles = customerService.CUSTOMERS_ROLES;
        $scope.isCollapsed = true;

        $scope.$on('$editCustomerCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editCustomerAdded', function () { $scope.isCollapsed = true;  });
        $scope.$on('$activeEventUpdated', function () { vm.eventInfo = eventsService.getActiveEvent(); });

        /** Adding */
        vm.customerAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#customerName').focus(); }, 100);
        };
        /** Update */
        vm.customerEdit = function (customer) {
            customerService.setCustomer(customer);
            $location.path('/updateCustomer');
            $timeout(function () {angular.element('#customerName').focus(); }, 100);
        };

        /** Delete */
        vm.customerDelete = function (customer) {
            var modalInstance = $uibModal
                .open( globalModalService
                    .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                        'OkCancelModalCtrl',
                        'Excluir Cliente',
                        'Confirma a exclusão deste Cliente?' )
                );
            modalInstance.result.then(function () {
                customerService
                    .deleteEventCustomer(customer)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        logger.logSuccess('Cliente apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Cliente.');
                        console.error(err);
                    });
            });
        };
        /** Inviting */
        vm.customerInvite = function () {
            var modalInstance = $uibModal
                .open( globalModalService
                    .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                        'OkCancelModalCtrl',
                        'Convidar Cliente',
                        'Deseja convidar os Clientes para o RSVP?' )
                );
            modalInstance.result.then(function () {
                customerService
                    .inviteEventCustomers()
                    .then( function() {
                        logger.logSuccess('Cliente convidado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao convidar Cliente.');
                        console.error(err);
                    });
            });
        };
        /** Send E-mail */
        vm.sendEmail =  function (customer) {
            if (!customer.email) {
                logger.logWarning('Cadastre o e-mail para compartilhar algo com seu Cliente.');
                return false;
            }

            emailModalService
                .emailModal(
                    'E-mail para ' + customer.name,
                    '',
                    customer.email,
                    ''
                )
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        logger.logError('Erro ao enviar e-mail.');
                        console.error(error);
                    }
                });
        }
    }

    function EditCustomerController($scope, $location, $rootScope, customerService, eventsService, logger) {
        var vm = this;
        vm.back = function () { $location.path('/customers'); };
        vm.editCustomer = customerService.getCustomer();

        if (angular.equals(vm.editCustomer, {}) ) { vm.back(); } /** Se não houver customer em memória volta pra lista */

        vm.editCustomer.birthDay = vm.editCustomer.birthDay ? vm.editCustomer.birthDay : '';

        var updateMainCustomers = function(customerName) {
            return new Promise(function (resolve, reject) {
                var event = eventsService.getActiveEvent();
                var eventType = event.type;

                var getName = function (role) {
                    var index = event.customers.findIndex( function (a) { return a.role === role; } );
                    if (index > -1)
                        return event.customers[index].name;
                    else
                        return false;
                };

                function getGroomName() {
                    return getName('Noivo');
                }
                function getBrideName() {
                    return getName('Noiva');
                }

                var mainCustomers;
                switch ( eventType ) {
                    case eventsService.eventTypesObject.CASAMENTO :
                    case eventsService.eventTypesObject.BODAS :
                        if(vm.editCustomer.role === 'Noiva') {
                            mainCustomers = customerName + ' e ' + getGroomName();  // TODO: traduzir -> AND
                        } else if(vm.editCustomer.role === 'Noivo') {
                            mainCustomers = getBrideName() + ' e ' + customerName;  // TODO: traduzir -> AND
                        }
                        break;
                    default:
                        mainCustomers = customerName;
                }
                event.mainCustomers = mainCustomers;

                eventsService
                    .updateEvent(event)
                    .then(function () {
                        resolve();
                    })
                    .catch(function (err) {
                        console.error(err);
                        reject(err);
                    });
            });
        };

        vm.cleanObj = function () {
            customerService.clearCustomer();
            vm.editCustomer = {};
            vm.editCustomer.birthDay = ''; /** evita a data padrão que é a data atual */
            vm.selectedItem = null;
            vm.searchText = null;
        };
        vm.canSubmit = function() {
            return true; /** permite todos os campos do formulário para não obrigar o campo nascimento, que deve ter uma data válida */
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editCustomerCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            customerService
                .editEventCustomer(vm.editCustomer)
                .then( function() {
                    updateMainCustomers(vm.editCustomer.name)
                        .then(function () {
                            vm.cleanObj();
                            $rootScope.$broadcast('$activeEventUpdatedLocally');
                            $scope.$emit('$editCustomerAdded');
                            logger.logSuccess('Cliente Incluído/atualizado com sucesso.');
                            vm.back();
                        })
                        .catch(function (err) {
                            logger.logError('Erro ao Incluir/editar Cliente.');
                            console.error(err);
                        })
                })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Cliente.');
                    console.error(err);
                });
        };

        /** Variáveis para md-autocomplete */
        vm.roles = customerService.CUSTOMERS_ROLES;
        vm.selectedItem = vm.editCustomer.role;
        vm.simulateQuery = false;
        vm.isDisabled = false;
        vm.noCache = true;
        /** Internal methods md-autocomplete */
        function createFilterFor(query) {
            return function filterFn(role) {
                return (angular.lowercase(role).indexOf(angular.lowercase(query)) > -1);
            };
        }
        function querySearch (query) {
            return query ? vm.roles.filter( createFilterFor(query) ) : vm.roles;
        }
        function searchTextChange(text) {
            vm.editCustomer.role = text;
        }
        function selectedItemChange(item) {
            vm.editCustomer.role = item;
        }
        vm.querySearch   = querySearch;
        vm.selectedItemChange = selectedItemChange;
        vm.searchTextChange   = searchTextChange;
    }

    angular
        .module('app.customers')
        .controller('CustomersController', ['$scope', '$uibModal', '$log', '$location', '$timeout', 'eventsService',
            'customerService', 'logger', 'globalModalService', 'authentication', 'emailModalService',
            CustomersController])
        .controller('EditCustomerController', [ '$scope', '$location', '$rootScope', 'customerService',
            'eventsService', 'logger', EditCustomerController]);

})();