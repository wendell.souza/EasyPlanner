/**
 * Created by wendell on 12/11/17.
 */
(function () {
    'use strict';

    angular
        .module('app.notes', []);
})();
