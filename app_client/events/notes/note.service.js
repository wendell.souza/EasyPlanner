/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';
    
    function noteService () {
        var _note = {};
        
        this.setNote = function (note) {
            _note = note;
        };
        
        this.getNote = function () {
            return _note;
        };
        
        this.clearNote = function  () {
            _note = {};
        };
    }
    
    angular
        .module('app.notes')
        .service('noteService', noteService);
    
})();