/**
 * Created by wendell on 30/01/18.
 */

( function () {  // wrap in IIFE (Immediately Invoked Function Expression)
    'use strict';
    function TasksController($scope, $location, $uibModal, $timeout, eventsService, taskService, logger, globalModalService, emailModalService) {
        let vm = this;
        $scope.isCollapsed = true;
        vm.eventInfo = eventsService.getActiveEvent();

        $scope.$on('$editTaskCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editTaskAdded', function () { $scope.isCollapsed = true; });
        $scope.$on('$activeEventUpdated', function () { vm.eventInfo = eventsService.getActiveEvent(); vm.sortByDate(); });

        /** Sorts Tasks */
        vm.sortByDate = function () {
            vm.eventInfo.tasks.sort( function(a, b) {
                return new Date(a.dueDate) - new Date(b.dueDate) ;
            });
        };
        vm.sortByDate();

        /** Adding */
        vm.taskAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#taskName').focus(); }, 100);
        };
        /** Update */
        vm.taskEdit = function (task) {
            taskService.setTask(task);
            $location.path('/updateTask');
            $timeout(function () {angular.element('#taskName').focus(); }, 100);
        };

        /** Delete */
        vm.taskDelete = function (task) {
            let modalInstance = $uibModal.open( globalModalService.getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Excluir Compromisso', 'Confirma a exclusão deste Compromisso?  ' + task.name ) );
            modalInstance.result.then(function () {
                eventsService.deleteEventTask(task)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        logger.logSuccess('Compromisso apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Compromisso.');
                        console.error(err);
                    });
            });
        };

        /** Toggles done */
        vm.toggleDone = function (task) {
            eventsService
                .editEventTask(task)
                .then()
                .catch(function (err) {
                    logger.logError('Erro ao finalizar Compromisso.');
                    console.error(err);
                });
        };

        vm.sendEmail =  function () {
            let scriptHtml = document.getElementById('tasks').innerHTML;
            emailModalService
                .emailModal(
                    'Compartilhar Tarefas' ,
                    '',
                    '',
                    '',
                    scriptHtml
                )
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        logger.logError('Erro ao enviar e-mail.');
                        console.error(error);
                    }
                });
        }
    }

    function EditTaskController($scope, $location, taskService, eventsService, logger) {
        let vm = this,
            hours,
            minutes;
        vm.back = function () { $location.path('/tasks'); };
        vm.editTask = taskService.getTask();
        if (angular.equals(vm.editTask, {}) ) { vm.back(); } /** Se não houver task em memória volta pra lista */
        vm.cleanObj = function () {
            taskService.clearTask();
            vm.editTask = {};
            vm.editTask.dueDate = null;
            vm.editingDate = null;
            vm.editingTime = null;
        };
        /** Inclusão - seta nulo */
        if (!vm.editTask._id) {
            vm.cleanObj();
        } else {
            hours   = new Date(vm.editTask.dueDate).getHours().toString();
            hours   = hours.length < 2 ? '0' + hours : hours;
            minutes = new Date(vm.editTask.dueDate).getMinutes().toString();
            minutes = minutes.length < 2 ? '0' + minutes : minutes;
            vm.editingTime = hours + ':' + minutes;
        }

        vm.canSubmit = function() {
            return $scope.formEditTask.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editTaskCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            vm.editTask.done = vm.editTask._id ? vm.editTask.done : false;
            vm.editTask.dueDate = vm.editTask.dueDate
                ? moment( new Date(vm.editTask.dueDate) )
                : null;
            if (vm.editingTime) {
                vm.editTask.dueDate
                    .set('hour', vm.editingTime.slice(0,2))
                    .set('minutes', vm.editingTime.slice(-2))
            }
            /** Não deixa data da compromisso no passado ou hoje */
            if ( moment(vm.editTask.dueDate) < moment() ) {
                logger.logError('Data do Compromisso deve ser no mínimo para amanhã.');
                return;
            }
            eventsService.editEventTask(vm.editTask)
                .then( function() {
                    vm.cleanObj();
                    $scope.$emit('$editTaskAdded');
                    logger.logSuccess('Compromisso Incluído/atualizado com sucesso.');
                    vm.back(); })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Compromisso.');
                    console.error(err);
                });
        };
    }

    angular
        .module('app.tasks')
        .controller('TasksController', ['$scope','$location', '$uibModal', '$timeout', 'eventsService', 'taskService', 'logger', 'globalModalService', 'emailModalService', TasksController])
        .controller('EditTaskController', [ '$scope', '$location', 'taskService', 'eventsService', 'logger', EditTaskController]);
}) ();