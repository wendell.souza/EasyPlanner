/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';
    
    function taskService () {
        var _task = {};
        
        this.setTask = function (task) {
            _task = task;
        };
        
        this.getTask = function () {
            return _task;
        };
        
        this.clearTask = function  () {
            _task = {};
        };
    }
    
    angular
        .module('app.tasks')
        .service('taskService', taskService);
    
})();