(function () {
    'use strict';

    function headerDashboard() {
        const directive = {
            restrict: 'C',
            controller: ['$scope', '$element', '$location', HeaderDashboardController]
        };

        return directive;

        function HeaderDashboardController($scope, $element, $location) {
            const path = function() {
                return $location.path();
            };

            const addBg = function(path) {
                $element.addClass('hide');
                $element.removeClass('nav-left-profile');

                switch (path) {
                    case '/dashboard':
                    case '/payment':
                        return $element.removeClass('hide');
                    case '/profile':
                        $element.removeClass('hide');
                        $element.addClass('nav-left-profile');
                        break;
                }
            };

            addBg($location.path());

            $scope.$watch(path, function() {
                // Não compara  pra garantir que pegará o último caminho, pq pode ter sido atualizado anteriormente e não atualiza da última vez
                return addBg($location.path());
            });
        }
    }
    
    function headerProfile() {
        const directive = {
            restrict: 'C',
            controller: ['$scope', '$element', '$location', HeaderProfileController]
        };

        return directive;

        function HeaderProfileController($scope, $element, $location) {
            const path = function() {
                return $location.path();
            };

            const addBg = function(path) {
                $element.addClass('hide');
                switch (path) {
                    case '/profile':
                    case '/payment':
                        return $element.removeClass('hide');
                }
            };

            addBg($location.path());

            $scope.$watch(path, function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                return addBg($location.path());
            });
        }
    }

    function headerEvent() {
        const directive = {
            restrict: 'C',
            controller: ['$scope', '$element', '$location', HeaderEventController]
        };

        return directive;

        function HeaderEventController($scope, $element, $location) {
            const path = function() {
                return $location.path();
            };

            const addBg = function(path) {
                $element.addClass('hide');
                switch (path) {
                    case '/customers':
                    case '/venues':
                    case '/notes':
                    case '/scripts':
                    case '/episodes':
                    case '/vendors':
                    case '/tasks':
                    case '/guests':
                        return $element.removeClass('hide');
                }
            };

            addBg($location.path());

            $scope.$watch(path, function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                return addBg($location.path());
            });
        }
    }
    
    function header() {
        const directive = {
            restrict: 'C',
            controller: ['$scope', '$element', '$location', HeaderController]
        };

        return directive;

        function HeaderController($scope, $element, $location) {
            const path = function() {
                return $location.path();
            };

            const addBg = function(path) {
                $element.removeClass('hide');
                switch (path) {
                    case '/rsvp' :
                    case '/confirmGuest' :
                    case '/updateRsvp' :
                        return $element.addClass('hide');
                }
            };

            addBg($location.path());

            $scope.$watch(path, function() {
                return addBg($location.path());
            });
        }
    }
    
    angular.module('app.mainScreen')
        .directive('headerDashboard', headerDashboard)
        .directive('headerProfile', headerProfile)
        .directive('headerEvent', headerEvent)
        .directive('header', header);

})();


