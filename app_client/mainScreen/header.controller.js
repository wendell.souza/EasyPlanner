/**
 * Created by wendell on 1/26/18.
 */
(function () {
    'use strict';
    
    function HeaderController($scope, $location, $uibModal, globalModalService, eventsService, logger) {
        var vm,
            updateData,
            path,
            hours,
            minutes;
        vm = this;

        var sumExpenses = function () {
            var total = 0;
            if (!vm.eventInfo.providers) {return;}
            vm.eventInfo.providers.forEach(function(provider) {
                if (provider.amount && provider.contracted) { total += provider.amount; }
            });
            return total;
        };

        updateData = function() {
            vm.eventInfo = eventsService.getActiveEvent();
            if ( !vm.eventInfo ) { return; }
            vm.showDate         = vm.eventInfo.date;
            vm.showTime         = vm.eventInfo.date;
            vm.showQtdGuests    = vm.eventInfo.guestConfirmedTotal + ' / ' + vm.eventInfo.guestTotal;
            vm.showQtdDetails   =
                'Adultos: '       + vm.eventInfo.adultsConfirmed + '/' + vm.eventInfo.adultsTotal
                + ' - Crianças: ' + vm.eventInfo.kidsConfirmed   + '/' + vm.eventInfo.kidsTotal
                + ' - Free: '     + vm.eventInfo.freeConfirmed   + '/' + vm.eventInfo.freeTotal;
            vm.showBudget       = vm.eventInfo.budget;
            vm.editingDate      = vm.eventInfo.date;
            vm.editingQtdGuests = vm.eventInfo.qtdGuests;
            vm.editingBudget    = vm.eventInfo.budget;

            hours   = new Date(vm.eventInfo.date).getHours().toString();
            hours   = hours.length < 2 ? '0' + hours : hours;
            minutes = new Date(vm.eventInfo.date).getMinutes().toString();
            minutes = minutes.length < 2 ? '0' + minutes : minutes;
            vm.editingTime = hours + ':' + minutes;
            vm.totalExpenses = sumExpenses();
        };
        path = function() { return $location.path(); };
        
        updateData();
        
        $scope.$watch(path, function(newVal, oldVal) {
            if (newVal === oldVal) { return; }
            return updateData();
        });

        $scope.$on('$activeEventUpdatedLocally', function () { updateData(); });
        
        vm.updateEventInfo = function () {
            var eventCopy = angular.extend({}, vm.eventInfo);
            eventCopy.date = moment( new Date(vm.editingDate) )
                    .set('hour', vm.editingTime.slice(0,2))
                    .set('minutes', vm.editingTime.slice(-2));
            eventCopy.qtdGuests = vm.editingQtdGuests;
            eventCopy.budget = vm.editingBudget;

            eventsService.updateEvent(eventCopy)
                .then(function () {
                    updateData();
                    logger.logSuccess('Evento atualizado com sucesso.');
                })
                .catch(function (err) {
                    logger.logError('Erro ao atualizar Evento.');
                    console.error(err);
                });
        };
    
        vm.archiveEvent = function() {
            var modalInstance = $uibModal.open( globalModalService
                .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                    'OkCancelModalCtrl',
                    'Arquivar Evento',
                    'Tem certeza que deseja arquivar esse Evento?' )
            );
            modalInstance.result.then(function () {
                eventsService
                    .archiveEvent( vm.eventInfo._id, { archive: true} )
                    .then( function () {
                        logger.logSuccess('Arquivado com sucesso!');
                        $location.path('/dashboard');
                    })
                    .catch( function(err) {
                        console.error(err);
                        logger.logSuccess('Erro ao arquivar evento!');
                    });
            });
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopenHour = !$scope.status.isopenHour;
        };

    }
    
    angular
        .module('app.mainScreen')
        .controller('HeaderController', ['$scope', '$location', '$uibModal', 'globalModalService', 'eventsService', 'logger', HeaderController]);
})();