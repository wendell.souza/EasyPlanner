(function () {
    'use strict';
    function sidebar() {
        const directive = {
            restrict: 'C',
            controller: ['$scope', '$element', '$location', 'authentication', SidebarDirectiveController]
        };

        return directive;

        function SidebarDirectiveController($scope, $element, $location, authentication) {
            let addBg, path, currentUser;
            
            currentUser = authentication.getCurrentUser();
            if (currentUser && currentUser.isCustomer) { // se usuário for Cliente não mostrar nenhum menu
                $element.addClass('hide');
            }
            
            path = function() {
                return $location.path();
            };

            addBg = function() {
                $element.removeClass('hide');

                if (currentUser && currentUser.isCustomer) { // se usuário for Cliente não mostrar nenhum menu
                    $element.addClass('hide');
                }
            };
            addBg();

            $scope.$watch(path, function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                return addBg($location.path());
            });
        }
    }

    angular.module('app.mainScreen')
        .directive('sidebar', sidebar);

})();
