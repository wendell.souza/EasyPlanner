const gulp = require('gulp');
const gulpMinify = require('gulp-babel-minify');
const gulpConcat = require('gulp-concat');

const jsFiles = [
    'app_client/core/app.module.js',
    'app_client/core/config.route.js',
    'app_client/core/app.controller.js',
    'app_client/core/app.service.js',
    'app_client/core/app.ui.service.js',
    'app_client/core/filters/angular-br-filters.min.js',
    'app_client/core/filters/angular-input-masks-standalone.min.js',
    'app_client/core/modal/okCancelModal/globalModal.controller.js',
    'app_client/core/modal/emailModal/emailModal.controller.js',
    'app_client/core/modal/emailModal/emailModal.service.js',
    'app_client/core/i18n.js',
    'app_client/core/layout/nav.module.js',
    'app_client/core/layout/nav.directive.js',
    'app_client/mainScreen/mainScreen.module.js',
    'app_client/mainScreen/sidebar.directive.js',
    'app_client/mainScreen/header.directive.js',
    'app_client/mainScreen/header.controller.js',
    'app_client/user/authentication/authentication.module.js',
    'app_client/user/authentication/authentication.service.js',
    'app_client/user/authentication/authentication.controller.js',
    'app_client/user/authentication/authentication.directive.js',
    'app_client/user/user.module.js',
    'app_client/user/user.service.js',
    'app_client/user/dashboard/dashboard.controller.js',
    'app_client/user/dashboard/newEvent.controller.js',
    'app_client/user/payment/payment.controller.js',
    'app_client/user/payment/paymentModal/paymentModal.service.js',
    'app_client/user/payment/paymentModal/paymentModal.controller.js',
    'app_client/user/profile/profile.controller.js',
    'app_client/events/events.module.js',
    'app_client/events/events.service.js',
    'app_client/events/config.events.js',
    'app_client/events/notes/notes.module.js',
    'app_client/events/notes/notes.controller.js',
    'app_client/events/notes/note.service.js',
    'app_client/events/episodes/episodes.module.js',
    'app_client/events/episodes/episodes.controller.js',
    'app_client/events/episodes/episodes.service.js',
    'app_client/events/episodes/scripts.module.js',
    'app_client/events/episodes/scripts.controller.js',
    'app_client/events/episodes/scripts.service.js',
    'app_client/events/vendors/providers.module.js',
    'app_client/events/vendors/providers.controller.js',
    'app_client/events/vendors/provider.service.js',
    'app_client/events/tasks/tasks.module.js',
    'app_client/events/tasks/tasks.controller.js',
    'app_client/events/tasks/task.service.js',
    'app_client/events/customers/customers.module.js',
    'app_client/events/customers/customers.controller.js',
    'app_client/events/customers/customer.service.js',
    'app_client/events/venues/venues.module.js',
    'app_client/events/venues/venues.controller.js',
    'app_client/events/venues/venues.service.js',
    'app_client/events/guests/guests.module.js',
    'app_client/events/guests/guests.controller.js',
    'app_client/events/guests/guests.service.js',
    'app_client/core/modal/tooltipModalRsvp/tooltipModalRsvp.service.js',
    'app_client/core/modal/tooltipModalRsvp/tooltipModalRsvp.controller.js'
];

gulp.task('minify', function() {

    gulp.src(jsFiles)
        .pipe(gulpConcat('easy.min.js'))
        .pipe(gulpMinify({
            mangle: {
                keppClassName: false
            }
        }))
        .pipe(gulp.dest('./app_client'));
});
