var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use( new LocalStrategy ({
        usernameField: 'email'
    },
    function(username, password, done) {
        User
            .findOne({ email: username, active: true }, function (err, user) {
                if (err) { return done(err) }
                if (!user) {
                    return done(null, false, {
                        message: 'Não encontramos esse e-mail =('
                    });
                }
                if (!user.validPassword( password )) {
                    return done(null, false, {
                        message: 'Oops! A senha está incorreta ...'
                    });
                }
                return done(null, user);
            });
    }
));