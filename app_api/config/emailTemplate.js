/**
 * Created by wendell on 10/5/17.
 */

module.exports.getEmailHtml = function (customerName, customerEmail, companyName, eventName, link) {
    return '<!DOCTYPE html>\n' +
        '<html>\n' +
        '<head>\n' +
        '<title>A Responsive Email Template</title>\n' +
        '<!--\n' +
        '\n' +
        '    An email present from your friends at Litmus (@litmusapp)\n' +
        '\n' +
        '    Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.\n' +
        '    It\'s highly recommended that you test using a service like Litmus (http://easyplanner.com.br) and your own devices.\n' +
        '\n' +
        '    Enjoy!\n' +
        '\n' +
        ' -->\n' +
        '<meta charset="utf-8">\n' +
        '<meta name="viewport" content="width=device-width, initial-scale=1">\n' +
        '<meta http-equiv="X-UA-Compatible" content="IE=edge" />\n' +
        '<style type="text/css">\n' +
        '    /* CLIENT-SPECIFIC STYLES */\n' +
        '    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */\n' +
        '    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */\n' +
        '    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */\n' +
        '\n' +
        '    /* RESET STYLES */\n' +
        '    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}\n' +
        '    table{border-collapse: collapse !important;}\n' +
        '    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}\n' +
        '\n' +
        '    /* iOS BLUE LINKS */\n' +
        '    a[x-apple-data-detectors] {\n' +
        '        color: inherit !important;\n' +
        '        text-decoration: none !important;\n' +
        '        font-size: inherit !important;\n' +
        '        font-family: inherit !important;\n' +
        '        font-weight: inherit !important;\n' +
        '        line-height: inherit !important;\n' +
        '    }\n' +
        '\n' +
        '    /* MOBILE STYLES */\n' +
        '    @media screen and (max-width: 525px) {\n' +
        '\n' +
        '        /* ALLOWS FOR FLUID TABLES */\n' +
        '        .wrapper {\n' +
        '          width: 100% !important;\n' +
        '            max-width: 100% !important;\n' +
        '        }\n' +
        '\n' +
        '        /* ADJUSTS LAYOUT OF LOGO IMAGE */\n' +
        '        .logo img {\n' +
        '          margin: 0 auto !important;\n' +
        '        }\n' +
        '\n' +
        '        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\n' +
        '        .mobile-hide {\n' +
        '          display: none !important;\n' +
        '        }\n' +
        '\n' +
        '        .img-max {\n' +
        '          max-width: 80% !important;\n' +
        '          width: 80% !important;\n' +
        '          height: auto !important;\n' +
        '        }\n' +
        '\n' +
        '        /* FULL-WIDTH TABLES */\n' +
        '        .responsive-table {\n' +
        '          width: 100% !important;\n' +
        '        }\n' +
        '\n' +
        '        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\n' +
        '        .padding {\n' +
        '          padding: 10px 5% 15px 5% !important;\n' +
        '        }\n' +
        '\n' +
        '        .padding-meta {\n' +
        '          padding: 30px 5% 0px 5% !important;\n' +
        '          text-align: center;\n' +
        '        }\n' +
        '\n' +
        '        .padding-copy {\n' +
        '             padding: 10px 5% 10px 5% !important;\n' +
        '          text-align: center;\n' +
        '        }\n' +
        '\n' +
        '        .no-padding {\n' +
        '          padding: 0 !important;\n' +
        '        }\n' +
        '\n' +
        '        .section-padding {\n' +
        '          padding: 50px 15px 50px 15px !important;\n' +
        '        }\n' +
        '\n' +
        '        /* ADJUST BUTTONS ON MOBILE */\n' +
        '        .mobile-button-container {\n' +
        '            margin: 0 auto;\n' +
        '            width: 100% !important;\n' +
        '        }\n' +
        '\n' +
        '        .mobile-button {\n' +
        '            padding: 15px !important;\n' +
        '            border: 0 !important;\n' +
        '            font-size: 16px !important;\n' +
        '            display: block !important;\n' +
        '        }\n' +
        '\n' +
        '    }\n' +
        '\n' +
        '    /* ANDROID CENTER FIX */\n' +
        '    div[style*="margin: 16px 0;"] { margin: 0 !important; }\n' +
        '</style>\n' +
        '</head>\n' +
        '<body style="margin: 0 !important; padding: 0 !important;">\n' +
        '\n' +
        '<!-- HIDDEN PREHEADER TEXT -->\n' +
        '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">\n' +
        '    Você foi convidado para participar da organização desse incrível evento ...\n' +
        '</div>\n' +
        '\n' +
        '<!-- HEADER -->\n' +
        '<table border="0" cellpadding="0" cellspacing="0" width="100%">\n' +
        '    <tr>\n' +
        '        <td bgcolor="#ffffff" align="center">\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">\n' +
        '            <tr>\n' +
        '            <td align="center" valign="top" width="500">\n' +
        '            <![endif]-->\n' +
        '            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">\n' +
        '                <tr>\n' +
        '                    <td align="center" valign="top" style="padding: 15px 0;" class="logo">\n' +
        '                        <a href="http://easyplanner.com.br" target="_blank">\n' +
        '                            <img alt="Easyplanner" src="https://afternoon-headland-69166.herokuapp.com/images/easy_planner_logo.png" width="300" height="75" class="img-max" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #000000; font-size: 40px;" border="0">\n' +
        '                        </a>' +
        '                    </td>\n' +
        '                </tr>\n' +
        '            </table>\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            </td>\n' +
        '            </tr>\n' +
        '            </table>\n' +
        '            <![endif]-->\n' +
        '        </td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td bgcolor="#ffffff" align="center" style="padding: 15px;">\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">\n' +
        '            <tr>\n' +
        '            <td align="center" valign="top" width="500">\n' +
        '            <![endif]-->\n' +
        '            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">\n' +
        '                <tr>\n' +
        '                    <td>\n' +
        '                        <!-- COPY -->\n' +
        '                        <table width="100%" border="0" cellspacing="0" cellpadding="0">\n' +
        '                            <tr>\n' +
        '                                <td align="center" style="font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy"> Olá ' + customerName + '! </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy"> ' + companyName + ' convidou você a participar da organização do evento: </td>\n' +
        '                            </tr>\n' +
        '                        </table>\n' +
        '                    </td>\n' +
        '                </tr>\n' +
        '            </table>\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            </td>\n' +
        '            </tr>\n' +
        '            </table>\n' +
        '            <![endif]-->\n' +
        '        </td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td bgcolor="#ffffff" align="center" style="padding: 15px;">\n' +
        '            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">\n' +
        '                <tr>\n' +
        '                    <td>\n' +
        '                        <table width="100%" border="0" cellspacing="0" cellpadding="0">\n' +
        '                            <tr>\n' +
        '                                <!-- COPY -->\n' +
        '                                <td align="center" style="font-size: 27px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy"> ' + eventName + ' </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <!-- COPY -->\n' +
        '                                <td align="center" style="padding: 30px 5px 10px 5px; font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #333333;"> Seu login de acesso é: </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;" class="padding-copy"> ' + customerEmail + ' </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <td align="center">\n' +
        '                                    <!-- BULLETPROOF BUTTON -->\n' +
        '                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">\n' +
        '                                        <tr>\n' +
        '                                            <td align="center" style="padding-top: 25px;" class="padding">\n' +
        '                                                <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">\n' +
        '                                                    <tr>\n' +
        '                                                        <td align="center" style="border-radius: 3px; background-color: rgba(72,130,169,0.72);" bgcolor="#256F9C"><a href="' + link + '" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; border-radius: 3px; padding: 15px 25px; display: inline-block;" class="mobile-button"> Crie aqui sua senha </a></td>\n' +
        '                                                    </tr>\n' +
        '                                                </table>\n' +
        '                                            </td>\n' +
        '                                        </tr>\n' +
        '                                    </table>\n' +
        '                                </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy"> Ou copie esse link para o seu navegador: </td>\n' +
        '                            </tr>\n' +
        '                            <tr>\n' +
        '                                <td align="center" style="padding: 20px 0 0 0; font-size: 10px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy"> ' + link + ' </td>\n' +
        '                            </tr>\n' +
        '                        </table>\n' +
        '                    </td>\n' +
        '                </tr>\n' +
        '            </table>\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            </td>\n' +
        '            </tr>\n' +
        '            </table>\n' +
        '            <![endif]-->\n' +
        '        </td>\n' +
        '    </tr>\n' +
        '    <tr>\n' +
        '        <td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">\n' +
        '            <tr>\n' +
        '            <td align="center" valign="top" width="500">\n' +
        '            <![endif]-->\n' +
        '            <!-- UNSUBSCRIBE COPY -->\n' +
        '            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">\n' +
        '                <tr>\n' +
        '                    <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">\n' +
        '                        <a href="http://easyplanner.com.br" target="_blank" style="color: #666666; text-decoration: none;"> © 2017 Easy Planner </a>\n' +
        '                    </td>\n' +
        '                </tr>\n' +
        '            </table>\n' +
        '            <!--[if (gte mso 9)|(IE)]>\n' +
        '            </td>\n' +
        '            </tr>\n' +
        '            </table>\n' +
        '            <![endif]-->\n' +
        '        </td>\n' +
        '    </tr>\n' +
        '</table>\n' +
        '\n' +
        '</body>\n' +
        '</html>\n';
};