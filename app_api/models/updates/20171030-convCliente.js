/**
 * Created by wendell on 10/30/17.
 * branch: convCliente
 * Atualiza todos os cliente para ativos para continuarmos usar os cliente de teste que existem atualmente
 */
db.users.updateMany({}, { $set: { active: true} });