/**
 * Created by wendell on 12/5/17.
 * branch: visu
 * Refatora o nome da coluna "locals" para "venue".
 */
db.events.updateMany( {}, { $rename: { "locals": "venues" } } );