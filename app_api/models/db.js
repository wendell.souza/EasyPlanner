var mongoose = require( 'mongoose' );

/** Set database URI */
const DBURI = process.env.MONGO_URI;
const DBSERVERNAME = DBURI.substr( DBURI.lastIndexOf('/', DBURI.length+2));
console.log('Mongoose is about to connect on ' + DBSERVERNAME );
mongoose.Promise = global.Promise;
mongoose.connect(DBURI, {
    promiseLibrary: global.Promise
});

mongoose.connection.on('connected', function() {
    console.log('Mongoose connected to ' + DBSERVERNAME);
});
mongoose.connection.on('error', function(err) {
    console.log('Mongoose connection error: ' + err);
});
mongoose.connection.on('disconnected', function() {
    console.log('Mongoose disconnected.');
});

var readLine = require ("readline"); 
if (process.platform === "win32") { 
    var rl = readLine.createInterface ({ 
        input: process.stdin, 
        output: process.stdout 
    }); 

    rl.on ("SIGINT", function () { 
        process.emit ("SIGINT"); 
    }); 
}

// CAPTURE APP TERMINATION / RESTART EVENTS
// To be called when process is restarted or terminated
gracefulShutdown = function(msg, callback) {
    mongoose.connection.close(function() {
        console.log('Mongoose disconnected through ' + msg);
        callback();
    });
};

// For nodemon restarts
process.once('SIGUSR2', function() {
    gracefulShutdown('nodemon restart', function() {
        process.kill(process.pid, 'SIGUSR2');
    });
});

// For app termination
process.on('SIGINT', function() {
    gracefulShutdown('app termination', function() {
        process.exit(0);
    });
});

// For Heroku app termination
process.on('SIGTERM', function() {
    gracefulShutdown('Heroku app termination', function() {
        process.exit(0);
    });
});

require('./events.js');
require('./user.js');