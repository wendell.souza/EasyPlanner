const mongoose = require( 'mongoose'),
    crypto = require('crypto'),
    jwt = require('jsonwebtoken'),
    Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const userSchema = new mongoose.Schema({
    email:          { type: String,  required: true, unique: true },
    name:           { type: String,  required: true },
    isCustomer:     { type: Boolean, default: false },
    company:        String,
    hash:           String,
    salt:           String,
    tempToken:      String,
    trial:          { type: Boolean, default: true },
    active:         { type: Boolean, default: false },
    plan:           String,
    stripeCustomer: Schema.Types.Mixed,
    created:        Date,
    updated:        Date
});

userSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(45).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha1').toString('hex');
};

userSchema.methods.setTempToken = function() {
    this.tempToken = crypto.randomBytes(45).toString('hex');
    return this.tempToken; // retorna para monstar o link pro Cliente
};

userSchema.methods.clearTempToken = function() {
    this.tempToken = null;
};

userSchema.methods.validPassword = function(password) {
    let hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha1').toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
    let expiry = new Date();
    expiry.setDate(expiry.getDate() + 15);
    return jwt.sign({
        _id        : this._id,
        email      : this.email,
        name       : this.name,
        company    : this.company,
        isCustomer : this.isCustomer,
        trial      : this.trial,
        exp        : parseInt( expiry.getTime() / 1000 )
    }, process.env.JWT_SECRET );
};

/**
 * Pre-save middleware
 */
userSchema.pre('save', function (next) {
    const now = new Date();
    // Sets creation and updated data on the User
    if (this.isNew) {
        this.created = now;
    }  else {
        this.updated = now;
    }
    next();
});


mongoose.model('User', userSchema);
