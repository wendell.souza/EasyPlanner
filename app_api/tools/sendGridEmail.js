/**
 * Sends email using SendGrid NodeJs Library
 * @param subject {String}
 * @param mailFrom {String}
 * @param mailTo {Array}
 * @param mailBcc {Array}
 * @param mailText {String}
 * @param pdfBuffer {Buffer}
 * */
module.exports.sendEmail = function (subject, mailFrom, mailTo, mailBcc, mailText, pdfBuffer) {
    return new Promise(function (resolve, reject) {
        if (!mailFrom) {
            reject({message: 'mailForm not found.'});
        } else if(!mailTo) {
            reject({message: 'mailTo not found.'});
        }
        /**
         * Using SendGrid's v3 Node.js Library:
         * https://github.com/sendgrid/sendgrid-nodejs
         */
        const sgMail = require('@sendgrid/mail');
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);

        let msgObj =
            {'personalizations': [{'to': [], 'bcc': []}],
                'from': {'email' : mailFrom},
                'subject': subject,
                'content': [{'type': 'text/plain', 'value': mailText}],

            };

        /** Inclui destinatários no campo To */
        if (typeof mailTo === 'object') {
            mailTo.forEach(function (item) {
                if (msgObj.personalizations[0].to.indexOf(item) < 0) { // só os que não existem ainda em qualquer campo (to, bcc, cco)
                    msgObj.personalizations[0].to.push(item);
                }
            });
        } else if (typeof mailTo === 'string') {
            msgObj.personalizations[0].to.push(mailTo);
        } else {
            reject({message: 'mailTo type unespected.'})
        }

        /** Inclui destinatários no campo Bcc */
        if (mailBcc) {
            mailBcc.forEach(function (item) {
                if (msgObj.personalizations[0].bcc.indexOf(item) < 0 && // só os que não existem ainda em qualquer campo (to, bcc, cco)
                    msgObj.personalizations[0].to.indexOf(item) < 0) {
                    msgObj.personalizations[0].bcc.push(item);
                }
            });
        }

        /** Verifica se tem anexo e inclui no objeto se positivo */
        if (pdfBuffer) {
            msgObj.attachments = [ {
                'content'     : pdfBuffer.toString('base64'),
                'filename'    : 'teste.pdf',
                'type'        : 'application/pdf'
            } ];
        }

        sgMail
            .send(msgObj, function(err, result) {
                if (err) {
                    console.error('Error from SendGrid: '+ err); // Logando para pegar a maior quantidade de informação possível do SendGrid
                    console.error(result);
                    reject(err);
                } else {
                    resolve();
                }
            })
    })
};
