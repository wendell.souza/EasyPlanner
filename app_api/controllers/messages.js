/**
 * Created by wendell on 5/15/17.
 */
'use strict';

var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONResponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id, user.email);
            });
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/** POST a new message, but must provided with a eventid */
/** path: /events/:eventid/messages */
module.exports.messagesCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id, user_email) {
            EventModel
                .findById(req.params.eventid)
                .or([ { user_id: user_id }, { "customers.email": user_email } ]) // permite o Cliente acessar seu evento
                .select('messages user_id')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONResponse(res, 404, err);
                        } else {
                            doAddmessages(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONResponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddmessages = function (req, res, event) {
    if (!event) {
        sendJSONResponse(res, 404, "eventid não encontrado.");
    } else {
        event.messages.push({
            message:  req.body.message,
            date:     req.body.date,
            customer: req.body.customer
        });
        event.save(function(err, event){
            var thisMessage;
            if (err) {
                sendJSONResponse(res, 404, err);
            } else {
                thisMessage = event.messages[event.messages.length -1];
                
                /** Emite evento para o chat */
                var messageToChat = {};

                /** Padrão das msg's enviadas :
                 * - msg de cliente tem o objeto cliente;
                 * - todas as msg's tem userId
                 * - todas as msg's tem eventId
                 *  */
                messageToChat.eventId = event._id;
                messageToChat.userId  = event.userId;
                messageToChat.message = thisMessage;
                
                var socketio = req.app.get('socketio'); // tacke out socket instance from the app container
                socketio.sockets.emit('newMessage', messageToChat); // emit an event for all connected clients
    
                /** Retorna objeto para o chamador da API */
                sendJSONResponse(res, 201, thisMessage);
            }
        });
    }
};

/** path: '/api/events/:eventid/messages/:messageid' */
module.exports.messagesUpdateOne = function (req, res ) {
    var thisMessage;
    if (!req.params.eventid || !req.params.messageid) {
        sendJSONResponse(res, 404, {
            "message": "Não encontrado, eventid e messageid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('messages')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONResponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    if (event.messages && event.messages.length > 0) {
                        thisMessage = event.messages.id(req.params.messageid);
                        if (!thisMessage) {
                            sendJSONResponse(res, 404, {
                                "message": "messageid not found"
                            });
                        } else {
                            thisMessage.message  = req.body.message;
                            thisMessage.date     = req.body.date;
                            thisMessage.customer = req.body.customer;
                        
                            event.save(function (err) {
                                if (err) {
                                    sendJSONResponse(res, 404, err);
                                } else {
                                    sendJSONResponse(res, 200, thisMessage);
                                }
                            });
                        }
                    } else {
                        sendJSONResponse(res, 404, {
                            "message": "Nenhum message para atualizar"
                        });
                    }
                }
            );
    });
};

/** path: 'app.delete('/api/events/:eventid/messages/:messageid' */
module.exports.messagesDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.messageid) {
        sendJSONResponse(res, 404, {
            "message": "Não encontrado, eventid e messageid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('messages')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONResponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    if (event.messages && event.messages.length > 0) {
                        if (!event.messages.id(req.params.messageid)) {
                            sendJSONResponse(res, 404, {
                                "message": "messageid not found"
                            });
                        } else {
                            event.messages.id(req.params.messageid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONResponse(res, 404, err);
                                } else {
                                    sendJSONResponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONResponse(res, 404, {
                            "message": "Nenhum compromisso para excluir"
                        });
                    }
                }
            );
    });
};
