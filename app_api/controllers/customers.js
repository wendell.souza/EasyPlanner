/**
 * Created by wendell on 4/26/17.
 */

var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');
var Email = require('../tools/sendGridEmail');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        'message': 'Usuário não encontrado.'
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user);
            });
    } else {
        sendJSONResponse(res, 404, {
            'message': 'Usuário não encontrado.'
        });
    }
};

/** POST a new customer, but must provided with a eventid */
/** path: /events/:eventid/customers */
module.exports.customersCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user){
            EventModel
                .findById(req.params.eventid)
                .where({ user_id: user._id })
                .select('customers')
                .exec(
                    function(err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddcustomers(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            'message':'Não encontrado, eventid obrigatório.'
        })
    }
};

var doAddcustomers = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, 'eventid não encontrado.');
    } else {
        event.customers.push({
            name:     req.body.name,
            lastName: req.body.lastName,
            role:     req.body.role,
            email:    req.body.email,
            birthDay: req.body.birthDay,
            phone:    req.body.phone,
            info:     req.body.info
        });
        event.save(function(err, event){
            var thisCustomer;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisCustomer = event.customers[event.customers.length -1];
                sendJSONresponse(res, 201, thisCustomer);
            }
        });
    }
};

/** path: '/api/events/:eventid/customers/:customerid' */
module.exports.customersUpdateOne = function (req, res ) {
    var thisCustomer;
    if (!req.params.eventid || !req.params.customerid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e customerid são obrigatórios'
        });
        return;
    }

    getUser(req, res, function(req, res, user) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user._id})
            .select('customers')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.customers && event.customers.length > 0) {
                        thisCustomer = event.customers.id(req.params.customerid);
                        if (!thisCustomer) {
                            sendJSONresponse(res, 404, {
                                'message': 'customerid not found'
                            });
                        } else {
                            thisCustomer.name     = req.body.name;
                            thisCustomer.lastName = req.body.lastName;
                            thisCustomer.role     = req.body.role;
                            thisCustomer.email    = req.body.email;
                            thisCustomer.birthDay = req.body.birthDay;
                            thisCustomer.phone    = req.body.phone;
                            thisCustomer.info     = req.body.info;

                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisCustomer);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhum customer para atualizar'
                        });
                    }
                }
            );
    });
};

/** path: 'app.delete('/api/events/:eventid/customers/:customerid' */
module.exports.customersDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.customerid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e customerid são obrigatórios'
        });
        return;
    }

    getUser(req, res, function(req, res, user) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user._id})
            .select('customers')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.customers && event.customers.length > 0) {
                        if (!event.customers.id(req.params.customerid)) {
                            sendJSONresponse(res, 404, {
                                'message': 'customerid not found'
                            });
                        } else {
                            event.customers.id(req.params.customerid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhuma cliente para excluir'
                        });
                    }
                }
            );
    });
};

/**
 * Gets the mais customer in the event baseas on type
 * @param {String} eventId
 * @param {function} callback function
 * @return {Object} An Object with Main Customers and all Customers name and Email in mailTo format
 * */
var getCustomersEmail = function (eventId, callback) {
    let allCustomers = [], customersObj;
    EventModel
        .findById(eventId)
        .select('-venues -episodes -tasks -vendors -messages -notes -guests')
        .exec(
            function (err, event) {
                if (!event) {
                    throw new Error('Evento não encontrado');
                } else if (err) {
                    throw new Error(err);
                }
                if (event.customers && event.customers.length > 0) {
                    const mainCustomers = event.mainCustomers;
                    event.customers.forEach(function (customer) {
                        if ( customer.name && customer.email ) {
                            allCustomers.push(customer.name + ' <' + customer.email + '>');
                        }
                    });
                    customersObj = {mainCustomers: mainCustomers, allCustomers: allCustomers};
                    callback(customersObj);
                } else {
                    throw new Error('Nenhum Cliente encontrado.');
                }
            }
        );
};

/**
 * Generates access key for the Customer and send to them a email invitation
 *
 * @param req.body.eventId {String }
 * @return JSON response object with 200 status code for invite succesfuly and 400 code for errors
 * */
module.exports.invite = function (req, res) {
    let ctrlApp = require('../controllers/app.js');
    let ctrlEvent = require('../controllers/events.js');
    let link, mailTo, mainCustomers;
    const server = ctrlApp.serverUrl();
    const mailFrom = 'Easyplanner <naoresponda@easyplanner.com.br>';

    if(!req.params.eventid) {
        sendJSONresponse(res, 400, {'code' : 4040004, 'message' : 'Campos obrigatórios para convite de Cliente: eventid.'});
        return;
    }

    getUser(req, res, function(req, res, user) {
        link = server + '/rsvpnoivos/';
        /** If the event doesn't has a Access Code generates it */
        ctrlEvent.getEventAccessCode(req.params.eventid, function (accessCode) {
            getCustomersEmail(req.params.eventid, function (customersObj) {
                mainCustomers = customersObj.mainCustomers;

                if (process.env.NODE_ENV !== 'production') {
                    mailTo = 'Wendell easyPlanner <wendell.souza@gmail.com>';
                } else {
                    mailTo = customersObj.allCustomers;
                }

                if (!mailTo || mailTo.length === 0 ) {
                    sendJSONresponse(res, 400, { 'message': 'Sem email cadastrado.'});
                    return;
                }
                /** Monta Txt **/
                const mailText = 'Olá!'
                    + '\n\n' + 'Você foi convidado para participar do RSVP do Casamento de ' + mainCustomers + '!'
                    + '\n\n' + 'Clique no link abaixo para acessar o RSVP: '
                    + '\n\n' + link + accessCode
                    + '\n\n' + 'Qualquer dúvida estamos à disposição.';
                Email.sendEmail('Convite para o RSVP de ' + mainCustomers, mailFrom, mailTo, null, mailText, null)
                    .then(function () {
                        sendJSONresponse(res, 204, null);
                    })
                    .catch(function (err) {
                        sendJSONresponse(res, 400, err);
                    });
            });
        });
    })
};
