var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.error(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/* POST a new script, but must provided with a eventid*/
/* /events/:eventid/scripts */
module.exports.scriptsCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('scripts')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddscripts(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddscripts = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, "eventid não encontrado.");
    } else {
        event.scripts.push({
            name:  req.body.name
        });
        event.save(function(err, event){
            var thisScript;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisScript = event.scripts[event.scripts.length -1];
                sendJSONresponse(res, 201, thisScript);
            }
        });
    }
};


/** path: '/api/events/:eventid/scripts/:scriptid' */
module.exports.scriptsUpdateOne = function (req, res ) {
    var thisScript;
    if (!req.params.eventid || !req.params.scriptid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e scriptid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('scripts')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.scripts && event.scripts.length > 0) {
                        thisScript = event.scripts.id(req.params.scriptid);
                        if (!thisScript) {
                            sendJSONresponse(res, 404, {
                                "message": "scriptid not found"
                            });
                        } else {

                            thisScript.name  = req.body.name;

                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisScript);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum script para atualizar"
                        });
                    }
                }
            );
    });
};

/** path: app.delete('/api/events/:eventid/scripts/:scriptid' */
module.exports.scriptsDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.scriptid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e scriptid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('scripts')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.scripts && event.scripts.length > 0) {
                        if (!event.scripts.id(req.params.scriptid)) {
                            sendJSONresponse(res, 404, {
                                "message": "scriptid not found"
                            });
                        } else {
                            event.scripts.id(req.params.scriptid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum script para excluir"
                        });
                    }
                }
            );
    });
};

/**
 * Adds new episode to a Script
 * @param: eventid {String} Event ID
 * @param: scriptid {String} Script ID
 * @path: '/api/events/:eventid/scripts/:scriptid/addepisode'
 * @method: POST
 * */
module.exports.scriptsAddEpisode = function (req, res ) {
    var thisScript;
    if (!req.params.eventid || !req.params.scriptid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e scriptid são obrigatórios"
        });
        return;
    }

    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('scripts')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        console.error(err);
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.scripts && event.scripts.length > 0) {
                        thisScript = event.scripts.id(req.params.scriptid);
                        if (!thisScript) {
                            sendJSONresponse(res, 404, {
                                "message": "scriptid not found"
                            });
                        } else {

                            thisScript.episodes.push({
                                name:  req.body.name,
                                hour:  req.body.hour,
                                notes: req.body.notes
                            });

                            event.save(function (err) {
                                if (err) {
                                    console.error(err);
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    var thisEdpisode = thisScript.episodes[thisScript.episodes.length -1];
                                    sendJSONresponse(res, 201, thisEdpisode);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum script para atualizar"
                        });
                    }
                }
            );
    });
};

/**
 * Updates an episode from a Script
 * @param: eventid {String} Event ID
 * @param: scriptid {String} Script ID
 * @param: episodeid {String} Episode ID
 * @path: '/api/events/:eventid/scripts/:scriptid/episodes/:episodeid'
 * @method: PUT
 * */
module.exports.scriptsUpdateEpisode = function (req, res ) {
    var thisScript, thisEpisode;
    if (!req.params.eventid || !req.params.scriptid || !req.params.episodeid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid, scriptid e episodeid são obrigatórios"
        });
        return;
    }

    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('scripts')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.scripts && event.scripts.length > 0) {

                        thisScript = event.scripts.id(req.params.scriptid);

                        if (!thisScript) {
                            sendJSONresponse(res, 404, {
                                "message": "scriptid not found"
                            });
                        } else {

                            thisEpisode = thisScript.episodes.id(req.params.episodeid);

                            if(!thisEpisode) {
                                sendJSONresponse(res, 404, {'message': 'Erro ao atualizar episode no Script: episodeid não econtrado'});
                            } else {

                                thisEpisode.name  = req.body.name;
                                thisEpisode.hour  = req.body.hour;
                                thisEpisode.notes = req.body.notes;

                                event.save(function (err) {
                                    if (err) {
                                        console.error(err);
                                        sendJSONresponse(res, 404, err);
                                    } else {
                                        sendJSONresponse(res, 200, thisEpisode);
                                    }
                                });
                            }

                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum script para atualizar"
                        });
                    }
                }
            );
    });
};

/**
 * Deletes an episode from a Script
 * @param: eventid {String} Event ID
 * @param: scriptid {String} Script ID
 * @param: episodeid {String} Episode ID
 * @path: '/api/events/:eventid/scripts/:scriptid/episodes/:episodeid'
 * @method: DELETE
 * */
module.exports.scriptsDeleteEpisode = function (req, res ) {
    var thisScript, thisEpisode;
    if (!req.params.eventid || !req.params.scriptid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid, scriptid e episodeid são obrigatórios"
        });
        return;
    }

    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('scripts')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.scripts && event.scripts.length > 0) {
                        thisScript = event.scripts.id(req.params.scriptid);
                        if (!thisScript) {
                            sendJSONresponse(res, 404, {
                                "message": "scriptid not found"
                            });
                        } else {
                            thisEpisode = thisScript.episodes.id(req.params.episodeid);
                            if(!thisEpisode) {
                                console.error('episodeid não econtrado');
                                sendJSONresponse(res, 404, {'message': 'Erro ao remover Episódio de Script: episodeid não econtrado'});
                            } else {
                                thisEpisode.remove();
                                event.save(function (err) {
                                    if (err) {
                                        console.error(err);
                                        sendJSONresponse(res, 404, err);
                                    } else {
                                        sendJSONresponse(res, 204, null);
                                    }
                                });
                            }

                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum script para apagar"
                        });
                    }
                }
            );
    });
};
