const mongoose = require('mongoose'),
    UserModel = mongoose.model('User'),
    StripeService = require('./stripe.service');

/**
 * Formata a resposta da API. // TODO: cirar uma só e usar essa na aplicação inteira.
 * @param res
 * @param status
 * @param content
 */
const sendJSONResponse = (res, status, content) => {
    res.status(status);
    res.json(content);
};

/**
 * Returns the found user on easyplanner DB or an error.
 * @param userId
 * @returns {Promise<user>}
 */
const getUser = (userId) => {
    return new Promise((resolve, reject) => {
        UserModel
            .findById(userId)
            .exec((err, user) => {
                if (!user) {
                    reject({ message: 'Usuário não encontrado.' });
                }
                else if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve(user);
            });
    });
};

/**
 * Cria Customer no Hub de pagamentos se ele não existir.
 * @returns {Promise<any>}
 * @param user
 * @param token
 */
const createCustomer = (user, token) => {
    return new Promise((resolve, reject) => {
        if (user.stripeCustomer) { // Se o customer já existe no Stripe simplesmente retorna o que já existe.
            resolve(user.stripeCustomer);
        }
        StripeService
            .createCustomer({
                email: user.email,
                token: token,
            })
            .then(customer => {
                resolve(customer);
            })
            .catch(err => {
                reject(err);
            })
    });
};

/**
 * Cria Assinatura no Hub de pagamento se o usuário não tiver ainda.
 * @param userPaymentId
 * @returns {Promise<any>}
 */
const subscribeCustomer = (stripeCustomer) => {
    return new Promise((resolve, reject) => {
        StripeService
            .customerHasActiveSubscription(stripeCustomer)
            .then(function (subscription) {
                if(subscription && subscription.plan.active) {
                    resolve(subscription);
                } else {
                    StripeService
                        .subscription(stripeCustomer)
                        .then(subscription => {
                            resolve(subscription);
                        })
                        .catch(err => {
                            reject(err);
                        })
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};

// TODO: substituir todos endpoints que requerem User, e usar o payload, que já tem todas infos do user!.

/**
 * Cria Usuário e Assinatura no Hub de pagamento.
 * @param req
 * @param res
 */
module.exports.subscription = (req, res) => {
    if (!req.payload.email || !req.payload._id || !req.body.token) {
        sendJSONResponse(res, 404, {message: 'token or user object not found.'});
        return;
    }

    getUser(req.payload._id)
        .then(user => {
            /** Se cliente não está criado no stripe requisita criação */
            createCustomer(user, req.body.token)
                .then(stripeCustomer => {
                    user.stripeCustomer = stripeCustomer; // Garante salvar novo customer se ele foi criado agora.
                    return subscribeCustomer(stripeCustomer);
                })
                .then(subscription => {
                    user.stripeCustomer.subscriptions.data[0] = subscription; // Salva a assinatura para futura validação e consulta mais fácil.
                    user.trial = false;        // Seta o usuário como assinante.
                    return user.save()
                })
                .then(userObj => {
                    const token = userObj.generateJwt();
                    sendJSONResponse(res, 200, {"token" : token});
                })
                .catch(err => {
                    sendJSONResponse(res, 500, err);
                });
        })
        .catch(err => {
            sendJSONResponse(res, 404, err);
        });
};

/**
 * Retorna histórico de Invoices do Usuário.
 * @param req
 * @param res
 */
module.exports.getHistoryInvoices = (req, res) => {
    if (!req.payload.email || !req.payload._id) {
        sendJSONResponse(res, 404, {message: 'user not found.'});
        return;
    }

    getUser(req.payload._id)
        .then(user => {
            StripeService
                .retrieveInvoiceByCustomer(user.stripeCustomer)
                .then(history => {
                    sendJSONResponse(res, 200, history);
                })
                .catch(err => {
                    sendJSONResponse(res, 500, err);
                });
        })
        .catch(err => {
            sendJSONResponse(res, 404, err);
        });
};


/**
 * Retorna Objeto Customer do Stripe.
 * @param req
 * @param res
 */
module.exports.getCustomer = (req, res) => {
    if (!req.payload.email || !req.payload._id) {
        sendJSONResponse(res, 404, {message: 'user not found.'});
        return;
    }

    getUser(req.payload._id)
        .then(user => {
            sendJSONResponse(res, 200, user.stripeCustomer);
        })
        .catch(err => {
            sendJSONResponse(res, 404, err);
        });
};

/**
 * Atualiza o cartão do Usuário no Hub de pagamento.
 * @param req
 * @param res
 */
module.exports.updateCard = (req, res) => {
    if (!req.payload.email || !req.payload._id || !req.body.token) {
        sendJSONResponse(res, 404, {message: 'token or user object not found.'});
        return;
    }

    getUser(req.payload._id)
        .then(user => {
            StripeService
                .updateCard({
                    customerId: user.stripeCustomer.id,
                    token: req.body.token,
                })
                .then(stripeCustomer => {
                    user.stripeCustomer = stripeCustomer;
                    return user.save()
                })
                .then(userObj => {
                    sendJSONResponse(res, 200, userObj.stripeCustomer);
                })
                .catch(err => {
                    sendJSONResponse(res, 500, err);
                });
        })
        .catch(err => {
            sendJSONResponse(res, 404, err);
        });
};

module.exports.getPaymentHubId = (req, res) => {
    sendJSONResponse(res, 200, { id: process.env.STRIPE_PUBLISHABLE_API_KEY });
};