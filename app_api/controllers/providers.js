/**
 * Created by wendell on 4/19/17.
 */

var mongoose = require('mongoose');
var EventModel = mongoose.model('Event')
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        'message': 'Usuário não encontrado.'
                    });
                    return;
                } else if (err) {
                    console.error(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            'message': 'Usuário não encontrado.'
        });
    }
};


/** POST a new provider, but must provided with a eventid */
/** path: /events/:eventid/vendors */
module.exports.providersCreate = function (req, res ) {
    if (req.params.eventid) {
        
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('providers')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddproviders(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            'message':'Não encontrado, eventid obrigatório.'
        })
    }
};

var doAddproviders = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, 'eventid não encontrado.');
    } else {
        event.providers.push({
            type:       req.body.type,
            name:       req.body.name,
            email:      req.body.email,
            phone:      req.body.phone,
            amount:     req.body.amount,
            details:    req.body.details,
            contracted: req.body.contracted
        });
        event.save(function(err, event){
            var thisProvider;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisProvider = event.providers[event.providers.length -1];
                sendJSONresponse(res, 201, thisProvider);
            }
        });
    }
};

/** path: '/api/events/:eventid/vendors/:providerid' */
module.exports.providersUpdateOne = function (req, res ) {
    var thisProvider;
    if (!req.params.eventid || !req.params.providerid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e providerid são obrigatórios'
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('providers')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.providers && event.providers.length > 0) {
                        thisProvider = event.providers.id(req.params.providerid);
                        if (!thisProvider) {
                            sendJSONresponse(res, 404, {
                                'message': 'providerid not found'
                            });
                        } else {
                            thisProvider.type       = req.body.type;
                            thisProvider.name       = req.body.name;
                            thisProvider.email      = req.body.email;
                            thisProvider.phone      = req.body.phone;
                            thisProvider.amount     = req.body.amount;
                            thisProvider.details    = req.body.details;
                            thisProvider.contracted = req.body.contracted;
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisProvider);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhum provider para atualizar'
                        });
                    }
                }
            );
    });
};

/** path: 'app.delete('/api/events/:eventid/vendors/:providerid' */
module.exports.providersDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.providerid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e providerid são obrigatórios'
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('providers')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.providers && event.providers.length > 0) {
                        if (!event.providers.id(req.params.providerid)) {
                            sendJSONresponse(res, 404, {
                                'message': 'providerid not found'
                            });
                        } else {
                            event.providers.id(req.params.providerid).remove();
                            event.save(function (err) {
                                if (err) {
                                    console.error(err);
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhum compromisso para excluir'
                        });
                    }
                }
            );
    });
};

/**
 * Receive a file as atachment to the provider
 * @path: '/events/:eventid/vendors/:providerid/uploadfile'
 * @param: eventid {String} Event ID
 * @param: providerid {String} Provider ID
 * */
module.exports.providersUploadFile = function(req, res) {
    var Busboy = require('busboy'),
        thisProvider,
        errorMsg = 'Erro ao fazer upload do arquivo de Fornecedor - providersUpload: ',
        Gridfs = require('gridfs-stream'),
        tamEx = false,
        fileSizeLimit = 1024*1024* 30; // 30MB
    
    var db = mongoose.connection.db;
    var mongoDriver = mongoose.mongo;
    var gfs = new Gridfs(db, mongoDriver);
    
    if (!req.params.eventid || !req.params.providerid) {
        sendJSONresponse(res, 404, {   'message': 'Não encontrado, eventid e providerid são obrigatórios' });
        return;
    }
    
    try {
        var busboy = new Busboy({
            headers: req.headers,
            limits : {
                files : 1,
                fileSize : fileSizeLimit
            }
        });
        req.pipe(busboy);
        busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            getUser(req, res, function (req, res, user_id) {
                EventModel
                    .findById(req.params.eventid)
                    .where({user_id: user_id})
                    .select('providers')
                    .exec(
                        function (err, event) {
                            if (!event) {
                                console.error(errorMsg + 'eventid não encontrado');
                                sendJSONresponse(res, 404, { 'message': 'eventid não encontrado' });
                                return;
                            } else if (err) {
                                console.error(errorMsg);
                                console.error(err);
                                sendJSONresponse(res, 400, err);
                                return;
                            }
                            if (event.providers && event.providers.length > 0) {
                                thisProvider = event.providers.id(req.params.providerid);
                                if (!thisProvider) {
                                    console.error(errorMsg + 'providerid not found');
                                    sendJSONresponse(res, 404, { 'message': 'providerid not found' });
                                } else {
                                    var writestream = gfs.createWriteStream({
                                        filename: filename,
                                        mode: 'w',
                                        content_type: mimetype,
                                        metadata: req.body
                                    });
                                    
                                    writestream.on('close', function(fileStream) {
                                        thisProvider.files.push({
                                            fileId:   fileStream._id,
                                            fileName: fileStream.filename
                                        });
                                        event.save(function (err) {
                                            if (err) {
                                                console.error(errorMsg);
                                                console.error(err);
                                                sendJSONresponse(res, 404, err);
                                            } else {
                                                var thisFile = thisProvider.files[thisProvider.files.length -1];
                                                sendJSONresponse(res, 201, thisFile);
                                            }
                                        });
                                    });
                                    file.pipe(writestream);
                                    file.on('limit', function () {
                                        tamEx = true;
                                        file.resume();
                                        file.unpipe(writestream);
                                        sendJSONresponse(res, 404, {'message': 'Tamanho máximo permitido: de ' + (fileSizeLimit/1024/1024).toString() + 'MB.'});
                                    });
                                }
                            } else {
                                sendJSONresponse(res, 404, {'message': 'Fornecedor não encontrado.'});
                            }
                        }
                    );
            });
        });
    }
    catch (err) {
        console.error('erro no catch do Busboy.');
        console.error(errorMsg + err);
        sendJSONresponse(res, 500, err);
    }
};

/** Gives a file atached to the provider */
/** path: '/events/:eventid/vendors/:providerid/downloadfile/:fileid' */
module.exports.providersDownloadFile = function(req, res) {
    var thisProvider,
        thisFile,
        errorMsg = 'Erro ao fazer download do arquivo de Fornecedor - providersDownload: ';
    /**
     * - Workaround erro do GridFs-Stream usando método depreciado do Mongo
     * https://github.com/aheckmann/gridfs-stream/issues/125.
     * */
    const Gridfs = require('gridfs-stream');
    eval(`Gridfs.prototype.findOne = ${Gridfs.prototype.findOne.toString().replace('nextObject', 'next')}`);

    var db = mongoose.connection.db;
    var mongoDriver = mongoose.mongo;
    var gfs = new Gridfs(db, mongoDriver);

    if (!req.params.eventid || !req.params.providerid) {
        sendJSONresponse(res, 404, { 'message': 'Não encontrado, eventid e providerid são obrigatórios'});
        return;
    }
    getUser(req, res, function (req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('providers')
            .exec(
                function (err, event) {
                    if (!event) {
                        console.error(errorMsg + 'eventid não encontrado');
                        sendJSONresponse(res, 404, {'message': 'eventid não encontrado'});
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.providers && event.providers.length > 0) {
                        thisProvider = event.providers.id(req.params.providerid);
                        if (!thisProvider) {
                            console.error(errorMsg + 'providerid not found');
                            sendJSONresponse(res, 404, { 'message': 'providerid not found' });
                        } else {
                            thisFile = thisProvider.files.id(req.params.fileid);
                            if (!thisFile) {
                                console.error(errorMsg +  'fileId não econtrado');
                                sendJSONresponse(res, 404, {'message': 'Erro ao encontrar arquivo de Fornecedor: fileId não econtrado'});
                            } else {
                                gfs.findOne({_id: thisFile.fileId}, function (err, file) {
                                    if (err) {
                                        console.error(errorMsg);
                                        console.error(err);
                                        sendJSONresponse(res, 400, err);
                                        return;
                                    }
                                    else if (!file) {
                                        console.error(errorMsg + 'fileId não encontrado no Banco de Dados');
                                        sendJSONresponse(res, 404, {'message': 'fileId não encontrado no Banco de Dados'});
                                        return;
                                    }
                                    
                                    res.set('content-type', file.contentType);
                                    res.set('content-disposition', 'attachment;'); //file.filename
                                    
                                    var readstream = gfs.createReadStream({
                                        _id: thisFile.fileId
                                    });
                                    
                                    readstream.on('error', function (err) {
                                        console.error(errorMsg);
                                        console.error(err);
                                        sendJSONresponse(res, 400, err);
                                    });
                                    readstream.pipe(res);
                                });
                            }
                        }
                    } else {
                        sendJSONresponse(res, 404, { 'message': 'Fornecedor não encontrado.' });
                    }
                }
            );
    });
};

/** Deletes a atached file */
/** path: '/events/:eventid/vendors/deletefile/:providerid/:fileid' */
module.exports.providersDeleteFile = function(req, res){
    var thisProvider,
        thisFile,
        errorMsg = 'Erro ao remover arquivo de Fornecedor - providersDeleteFile: ',
        Gridfs = require('gridfs-stream');

    var db = mongoose.connection.db;
    var mongoDriver = mongoose.mongo;
    var gfs = new Gridfs(db, mongoDriver);


    if (!req.params.eventid || !req.params.providerid) {
        sendJSONresponse(res, 404, {   'message': 'Não encontrado, eventid e providerid são obrigatórios' });
        return;
    }
    
    getUser(req, res, function (req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('providers')
            .exec(
                function (err, event) {
                    if (!event) {
                        console.error(errorMsg + 'eventid não encontrado');
                        sendJSONresponse(res, 404, { 'message': 'eventid não encontrado' });
                        return;
                    } else if (err) {
                        console.error(errorMsg + err);
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.providers && event.providers.length > 0) {
                        thisProvider = event.providers.id(req.params.providerid);
                        if (!thisProvider) {
                            console.error(errorMsg + 'providerid not found');
                            sendJSONresponse(res, 404, { 'message': 'providerid not found' });
                        } else {
                            thisFile = thisProvider.files.id(req.params.fileid);
                            if (!thisFile) {
                                console.error(errorMsg +  'fileId não econtrado');
                                sendJSONresponse(res, 404, {'message': 'Erro ao remover arquivo de Fornecedor: fileId não econtrado'});
                            } else {
                                gfs.remove({ _id : thisFile.fileId }, function(err){
                                    if(err) {
                                        console.error(errorMsg);
                                        console.error(err);
                                        sendJSONresponse(res, 404, err);
                                    } else {
                                        thisFile.remove();
                                        event.save(function (err) {
                                            if (err) {
                                                console.error(errorMsg);
                                                console.error(err);
                                                sendJSONresponse(res, 404, err);
                                            } else {
                                                sendJSONresponse(res, 204, null);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Fonecedor não encontrado.'
                        });
                    }
                }
            );
    });
};
