/**
 * Created by wendell on 21/02/17.
 */

var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/** POST a new task, but must provided with a eventid */
/** path: /events/:eventid/tasks */
module.exports.tasksCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('tasks')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddtasks(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddtasks = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, "eventid não encontrado.");
    } else {
        event.tasks.push({
            name:    req.body.name,
            dueDate: req.body.dueDate,
            done:    req.body.done,
            info:    req.body.info
          
        });
        event.save(function(err, event){
            var thisTask;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisTask = event.tasks[event.tasks.length -1];
                sendJSONresponse(res, 201, thisTask);
            }
        });
    }
};

/** path: '/api/events/:eventid/tasks/:taskid' */
module.exports.tasksUpdateOne = function (req, res ) {
    var thisTask;
    if (!req.params.eventid || !req.params.taskid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e taskid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('tasks')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.tasks && event.tasks.length > 0) {
                        thisTask = event.tasks.id(req.params.taskid);
                        if (!thisTask) {
                            sendJSONresponse(res, 404, {
                                "message": "taskid not found"
                            });
                        } else {
                            
                            thisTask.name    = req.body.name;
                            thisTask.dueDate = req.body.dueDate;
                            thisTask.done    = req.body.done;
                            thisTask.info    = req.body.info;
                            
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisTask);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum task para atualizar"
                        });
                    }
                }
            );
    });
};

/** path: 'app.delete('/api/events/:eventid/tasks/:taskid' */
module.exports.tasksDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.taskid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e taskid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('tasks')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.tasks && event.tasks.length > 0) {
                        if (!event.tasks.id(req.params.taskid)) {
                            sendJSONresponse(res, 404, {
                                "message": "taskid not found"
                            });
                        } else {
                            event.tasks.id(req.params.taskid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum compromisso para excluir"
                        });
                    }
                }
            );
    });
};
