var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/** POST a new venue, but must provided with a eventid*/
/** path:'/events/:eventid/venues' */
module.exports.venuesCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('venues')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddVenues(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddVenues = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, "eventid não encontrado.");
    } else {
        event.venues.push({
            name    : req.body.name,
            address : req.body.address,
            city    : req.body.city,
            state   : req.body.state,
            zipCode : req.body.zipCode
        });
        event.save(function(err, event){
            var thisVenue;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisVenue = event.venues[event.venues.length -1];
                sendJSONresponse(res, 201, thisVenue);
            }
        });
    }
};

/** path: '/api/events/:eventid/venues/:venueid' */
module.exports.venuesUpdateOne = function (req, res ) {
    var thisVenue;
    if (!req.params.eventid || !req.params.venueid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e venueid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('venues')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.venues && event.venues.length > 0) {
                        thisVenue = event.venues.id(req.params.venueid);
                        if (!thisVenue) {
                            sendJSONresponse(res, 404, {
                                "message": "venueid not found"
                            });
                        } else {
                            thisVenue.name    = req.body.venueName;
                            thisVenue.name    = req.body.name;
                            thisVenue.address = req.body.address;
                            thisVenue.city    = req.body.city;
                            thisVenue.state   = req.body.state;
                            thisVenue.zipCode = req.body.zipCode;
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisVenue);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum venue para atualizar"
                        });
                    }
                }
            );
    });
};

/** path: 'app.delete('/api/events/:eventid/venues/:venueid' */
module.exports.venuesDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.venueid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e venueid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('venues')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.venues && event.venues.length > 0) {
                        if (!event.venues.id(req.params.venueid)) {
                            sendJSONresponse(res, 404, {
                                "message": "venueid not found"
                            });
                        } else {
                            event.venues.id(req.params.venueid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum venue para excluir"
                        });
                    }
                }
            );
    });
};
