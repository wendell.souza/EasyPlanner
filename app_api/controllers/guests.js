/**
 * Created by wendell on 3/10/18.
 */

var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');


var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

/**
 * @callback {function} callback -
 * */
var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else if (!req.payload) {
        /** Acesso para clientes com código/senha do evento */
        // TODO: colocar autenticação BASIC
        callback(req,res, null);
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/** POST a new guest, but must provided with a eventid */
/** path: /events/:eventid/guests */
module.exports.guestsCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({ user_id: user_id })
                .select('guests')
                .exec(
                    function(err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddguests(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddguests = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, "eventid não encontrado.");
    } else {
        event.guests.push({
            name:               req.body.name,
            email:              req.body.email,
            phone:              req.body.phone,
            tableNumber:        req.body.tableNumber,
            notes:              req.body.notes,
            qtyAdults:          req.body.qtyAdults,
            qtyKids:            req.body.qtyKids,
            qtyFree:            req.body.qtyFree /** Confirmed always starts with 0 */
        });
        event.save(function(err, event){
            var thisGuest;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisGuest = event.guests[event.guests.length -1];
                sendJSONresponse(res, 201, thisGuest);
            }
        });
    }
};

/** path:  '/api/events/:eventid/guests/:guestid'  */
module.exports.guestsUpdateOne = function (req, res ) {
    var thisGuest;
    if (!req.params.eventid || !req.params.guestid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e guestid são obrigatórios"
        });
        return;
    }

    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('guests')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.guests && event.guests.length > 0) {
                        thisGuest = event.guests.id(req.params.guestid);
                        if (!thisGuest) {
                            sendJSONresponse(res, 404, {
                                "message": "guestid not found"
                            });
                        } else {
                            thisGuest.name               = req.body.name;
                            thisGuest.email              = req.body.email;
                            thisGuest.phone              = req.body.phone;
                            thisGuest.tableNumber        = req.body.tableNumber;
                            thisGuest.notes              = req.body.notes;
                            thisGuest.qtyAdults          = req.body.qtyAdults;
                            thisGuest.qtyKids            = req.body.qtyKids;
                            thisGuest.qtyFree            = req.body.qtyFree;
                            thisGuest.qtyAdultsConfirmed = req.body.qtyAdultsConfirmed;
                            thisGuest.qtyKidsConfirmed   = req.body.qtyKidsConfirmed;
                            thisGuest.qtyFreeConfirmed   = req.body.qtyFreeConfirmed;
                            thisGuest.confirmed          = req.body.confirmed;

                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, event);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum guest para atualizar"
                        });
                    }
                }
            );
    });
};

module.exports.guestsDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.guestid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e guestid são obrigatórios"
        });
        return;
    }

    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('guests')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.guests && event.guests.length > 0) {
                        if (!event.guests.id(req.params.guestid)) {
                            sendJSONresponse(res, 404, {
                                "message": "guestid not found"
                            });
                        } else {
                            event.guests.id(req.params.guestid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum compromisso para excluir"
                        });
                    }
                }
            );
    });
};

/************************************** METHODS EXPOSED FOR THE GUESTS   *******************************************/
module.exports.guestsReadOneByName = function(req, res) {
    if ( !req.params || !req.params.guestname || !req.body.eventDate) {
        sendJSONresponse(res, 404, {'message' : 'Nome e data do evento são obrigatórios.'});
        return;
    }

    // Pega horário no time zone do Brasil - TODO: configurar para novos regiões
    var moment = require('moment-timezone');
    var startDate = moment.tz(new Date(req.body.eventDate), 'America/Sao_Paulo');
    var endDate   = moment.tz(new Date(startDate), 'America/Sao_Paulo');
    
    startDate.second(0);
    startDate.hours(0);
    startDate.minute(0);

    endDate.hour(23);
    endDate.minute(59);
    endDate.second(59);

    console.info( startDate)
    console.info( endDate)

    console.info( new Date().getTimezoneOffset() )

    EventModel
        .find({
            date: {
                $gt: startDate,
                $lt: endDate
            } }, { 'guests.$' : 1, mainCustomers: 1, type: 1 })
        .where({ 'guests.name': req.params.guestname })
        .exec(function(err, event) {
            if (!event) {
                console.error(err);
            } else if ( event.length < 1 ) {
                sendJSONresponse( res, 404, { 'message' : 'Evento não encontrado.'});
                return;
            } else if (err) {
                console.error(err);
                sendJSONresponse( res, 404, err);
                return;
            } else if (event.length > 1 ) {
                console.error('Consulta de Convidados retornou mais de um evento - Consulta: ' + req.params.guestname + '. Eventos:' + event);
                sendJSONresponse( res, 404, { 'message': 'Consulta retornou mais de um evento.'});
                return;
            }
            sendJSONresponse(res, 200, event);
        });
};

module.exports.guestsRsvp = function(req, res) {
    var thisGuest;
    if (!req.params.eventid || !req.params.guestid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e guestid são obrigatórios"
        });
        return;
    }

    EventModel
        .findById(req.params.eventid)
        .select('guests')
        .exec(
            function (err, event) {
                if (!event) {
                    sendJSONresponse(res, 404, {
                        "message": "eventid não encontrado"
                    });
                    return;
                } else if (err) {
                    sendJSONresponse(res, 400, err);
                    return;
                }
                if (event.guests && event.guests.length > 0) {
                    thisGuest = event.guests.id(req.params.guestid);
                    if (!thisGuest) {
                        sendJSONresponse(res, 404, {
                            "message": "guestid not found"
                        });
                    } else {
                        thisGuest.qtyAdultsConfirmed = req.body.qtyAdultsConfirmed;
                        thisGuest.qtyKidsConfirmed   = req.body.qtyKidsConfirmed;
                        thisGuest.qtyFreeConfirmed   = req.body.qtyFreeConfirmed;
                        thisGuest.confirmed          = req.body.confirmed;

                        event.save(function (err) {
                            if (err) {
                                sendJSONresponse(res, 404, err);
                            } else {
                                sendJSONresponse(res, 200, thisGuest);
                            }
                        });
                    }
                } else {
                    sendJSONresponse(res, 404, {
                        "message": "Nenhum guest para atualizar"
                    });
                }
            }
        );

};

/************************************** METHODS EXPOSED FOR THE CUSTOMERS *******************************************/
module.exports.guestsReadByCode = function(req, res) {
    if ( !req.params || !req.params.accesscode ) {
        sendJSONresponse(res, 404, {'message' : 'Código é obrigatório.'});
        return;
    }
    EventModel
        .find({ accessCode: req.params.accesscode})
        .select('-venues -episodes -tasks -vendors -customers -messages -notes')
        .exec(function(err, event) {
            if ( event.length < 1 ) {
                sendJSONresponse( res, 404, { 'message' : 'Evento não encontrado.'});
                return;
            } else if (err) {
                sendJSONresponse( res, 404, err);
                return;
            }
            sendJSONresponse(res, 200, event);
        });
};

/**
 * POST a new guest, but must provided with a eventid
 * @param req.params.eventid
 * @path: /guest/events/:eventid/guests
 /* */
module.exports.guestsCreateCustomer = function (req, res ) {
    if (req.params.eventid) {
        EventModel
            .findById(req.params.eventid)
            .select('guests')
            .exec(
                function(err, event) {
                    if (err) {
                        sendJSONresponse(res, 404, err);
                    } else {
                        doAddguests(req, res, event);
                    }
                }
            )
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

/**
 * @path:  '/guest/events/:eventid/guests/:guestid'
 * */
module.exports.guestsUpdateOneCustomer = function (req, res ) {
    var thisGuest;
    if (!req.params.eventid || !req.params.guestid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e guestid são obrigatórios"
        });
        return;
    }

    EventModel
        .findById(req.params.eventid)
        .select('guests')
        .exec(
            function (err, event) {
                if (!event) {
                    sendJSONresponse(res, 404, {
                        "message": "eventid não encontrado"
                    });
                    return;
                } else if (err) {
                    sendJSONresponse(res, 400, err);
                    return;
                }
                if (event.guests && event.guests.length > 0) {
                    thisGuest = event.guests.id(req.params.guestid);
                    if (!thisGuest) {
                        sendJSONresponse(res, 404, {
                            "message": "guestid not found"
                        });
                    } else {
                        thisGuest.name               = req.body.name;
                        thisGuest.email              = req.body.email;
                        thisGuest.phone              = req.body.phone;
                        thisGuest.tableNumber        = req.body.tableNumber;
                        thisGuest.notes              = req.body.notes;
                        thisGuest.qtyAdults          = req.body.qtyAdults;
                        thisGuest.qtyKids            = req.body.qtyKids;
                        thisGuest.qtyFree            = req.body.qtyFree;
                        thisGuest.qtyAdultsConfirmed = req.body.qtyAdultsConfirmed;
                        thisGuest.qtyKidsConfirmed   = req.body.qtyKidsConfirmed;
                        thisGuest.qtyFreeConfirmed   = req.body.qtyFreeConfirmed;
                        thisGuest.confirmed          = req.body.confirmed;

                        event.save(function (err) {
                            if (err) {
                                sendJSONresponse(res, 404, err);
                            } else {
                                sendJSONresponse(res, 200, thisGuest);
                            }
                        });
                    }
                } else {
                    sendJSONresponse(res, 404, {
                        "message": "Nenhum guest para atualizar"
                    });
                }
            }
        );
};

/**
 * Expose the delete Guest method for the Customer
 * @url '/guest/events/:eventid/guests/:guestid'
 * @method DELETE
 * */
module.exports.guestsDeleteOneCustomer = function(req, res) {
    if (!req.params.eventid || !req.params.guestid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e guestid são obrigatórios"
        });
        return;
    }

    EventModel
        .findById(req.params.eventid)
        .select('guests')
        .exec(
            function (err, event) {
                if (!event) {
                    sendJSONresponse(res, 404, {
                        "message": "eventid não encontrado"
                    });
                    return;
                } else if (err) {
                    sendJSONresponse(res, 400, err);
                    return;
                }
                if (event.guests && event.guests.length > 0) {
                    if (!event.guests.id(req.params.guestid)) {
                        sendJSONresponse(res, 404, {
                            "message": "guestid not found"
                        });
                    } else {
                        event.guests.id(req.params.guestid).remove();
                        event.save(function (err) {
                            if (err) {
                                sendJSONresponse(res, 404, err);
                            } else {
                                sendJSONresponse(res, 204, null);
                            }
                        });
                    }
                } else {
                    sendJSONresponse(res, 404, {
                        "message": "Nenhum compromisso para excluir"
                    });
                }
            }
        );
};
