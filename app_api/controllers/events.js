var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONResponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.eventsCreate = function (req, res ) {
    var qtdeEvents;
    getUser(req, res, function(req, res, user) {
        /**
         * Verifica se cliente está usando trial e se for o caso só permite criar 2 eventos
         * */
        qtdeEvents = EventModel.find({ "user_id" : user._id }).count();

        console.info('qtde de evetnos: ' + qtdeEvents);

        if (user.trial && qtdeEvents >= 2 ) {
            sendJSONResponse(res, 400, { 'message': 'Quantidade de eventos no limite para período trial.' });
        } else {
            EventModel.create ({
                user_id       : user._id, // received from the callback
                type          : req.body.type,
                date          : req.body.date,
                info          : req.body.info,
                qtdGuests     : req.body.qtdGuests,
                mainCustomers : req.body.mainCustomers
            }, function (err, event) {
                if (err) {
                    console.log('error: ' + err);
                    sendJSONResponse(res, 400, err);
                } else {
                    sendJSONResponse(res, 201, event);
                }
            });
        }
    });
};

// TODO: colocar essa função em um lugar só, está repetido em outros controllers
var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        'message': 'Usuário não encontrado.'
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user);
            });
    } else {
        sendJSONResponse(res, 404, {
            'message': 'Usuário não encontrado.'
        });
    }
};

/**
 * Gets the informations in one object Event
 * @path '/events/:eventid'
 * @params req.params.eventid
 * */
module.exports.eventsReadOne = function(req, res) {
    if ( !req.params && !req.params.eventid ) {
        sendJSONResponse(res, 404, {'message' : 'Não foi encontrado o Id do Evento na requisição.'});
        return;
    }
    getUser(req, res, function(req, res, user) {
        EventModel
            .findById(req.params.eventid)
            .or([ { user_id: user._id }, { 'customers.email': user.email } ]) // permite o Cliente acessar seu evento
            .sort('episodes.hour')
            .exec(function(err, event) {
                if ( !event ) {
                    sendJSONResponse( res, 404, { 'message' : 'Evento não encontrado.'});
                    return;
                } else if (err) {
                    sendJSONResponse( res, 404, err);
                    return;
                }
                sendJSONResponse(res, 200, event);
            });
    });
};

/**
 * Return the user event list
 * @method: GET
 * @path: '/users/events'
 * */
module.exports.eventsListByUser = function (req, res ) {
    getUser(req, res, function(req, res, user) {
        EventModel
            .find()
            .where({ user_id: user._id }) // permite o Cliente acessar seu evento
            .sort('date')
            .exec(function(err, event) {
                if ( !event ) {
                    sendJSONResponse( res, 404, { 'message' : 'Evento(s) não encontrado(s).'});
                    return;
                } else if (err) {
                    sendJSONResponse( res, 404, err);
                    return;
                }
                sendJSONResponse(res, 200, event);
            });
    });
};

/**
 * Returns the quantity of events that one use has
 * @method: GET
 * @path: '/users/events'
 * */
module.exports.eventsQtdyByUser = function (req, res ) {
    getUser(req, res, function(req, res, user) {
        EventModel
            .find({ "user_id" : user._id })
            .count()
            .exec(function(err, qtdy) {
                if ( !qtdy ) {
                    sendJSONResponse( res, 200, 0);
                    return;
                } else if (err) {
                    sendJSONResponse( res, 404, err);
                    return;
                }
                sendJSONResponse(res, 200, qtdy);
            });
    });
};

/** path: '/users/events/archived' */
module.exports.eventsArchivedByUser = function (req, res ) {
    getUser(req, res, function(req, res, user) {
        EventModel
            .find()
            .where({ user_id: user._id })
            .and({ archived: true} )
            .sort('date')
            .exec(function(err, event) {
                if ( !event ) {
                    sendJSONResponse( res, 404, { 'message' : 'Evento(s) não encontrado(s).'});
                    return;
                } else if (err) {
                    sendJSONResponse( res, 404, err);
                    return;
                }
                sendJSONResponse(res, 200, event);
            })
    });
};

/** path: '/api/events/:eventid' */
module.exports.eventsUpdateOne = function (req, res ) {
    if (!req.params.eventid) {
        sendJSONresponse(res, 404, {
            'message': 'Evento não encontrado, eventid é obrigatório.'
        });
        return;
    }
    getUser(req, res, function (req, res, user){
        EventModel
            .findById(req.params.eventid)
            .where({ user_id: user._id })
            .select('-venues -episodes -tasks -vendors -customers -messages -notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONResponse(res, 404, {
                            'message': 'eventid not found'
                        });
                        return;
                    } else if (err) {
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    event.type          = req.body.type;
                    event.date          = req.body.date;
                    event.closed        = req.body.closed;
                    event.info          = req.body.info;
                    event.qtdGuests     = req.body.qtdGuests;
                    event.mainCustomers = req.body.mainCustomers;
                    event.budget        = req.body.budget;
                    event.save(function(err, event) {
                        if (err) {
                            console.error(err);
                            sendJSONResponse(res, 400, err);
                        } else {
                            sendJSONResponse(res, 200, event);
                        }
                    });
                }
            );
    });
};

/**
 * Generates random key for users access use, for exemple, in the RSVP
 * @retun {function} callback function with 6 string random key.
 * */
var generateRandomKey = function (callback) {
    var RandomOrg = require('random-org');
    /** Response exemples from random-org:
     * { random: { data: [ 'MRKVW2' ], completionTime: '2018-03-24 21:36:11Z' },
     *     bitsUsed: 31,
     *     bitsLeft: 249907,
     *     requestsLeft: 997,
     *     advisoryDelay: 920 }
     * */

    var randomParams = {
        /* Required */
        n: 1,
        // The number of random strings to generate (valid values: [1-10000]).
        length: 6,
        // The length of each string you'd like generated.
        characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        // The set of characters allowed to appear in the generated strings (maximum length: 80).
        // Unicode characters are supported.

        /* Optional */
        // replacement: Boolean
        // Whether or not the generated numbers can contain duplicates (default: true).
    };

    var random = new RandomOrg({ apiKey: process.env.RANDOM_ORG });
    random.generateStrings(randomParams)
        .then(function (data) {
            callback(data.random.data);
        })
        .catch( function (err) {
            console.error( err);
            return new Error('Erro ao gerar chave de random-org: ' + err);
        });
};

/**
 * Verify if  the Event  has AccessCode
 * @param eventId {String} Event Id
 * @param {function} callback function(accessCode)
 */
module.exports.getEventAccessCode = function (eventId, callback) {
    if (!eventId) {
        throw new Error('eventid é obrigatório.');
    }
    EventModel
        .findById(eventId)
        .select('-venues -episodes -tasks -vendors -customers -messages -notes -guests')
        .exec(
            function (err, event) {
                if (!event) {
                    throw new Error('Evento não encontrado.');
                } else if (err) {
                    throw new Error(err);
                } else if (event.accessCode) {
                    callback(event.accessCode);
                } else {
                    generateRandomKey(function (code) {
                        event.accessCode = code;
                        event.save( function (err) {
                            if(err) {
                                throw new Error(err);
                            } else {
                                callback(code);
                            }
                        });
                    });
                }
            }
        );
};

/** path: '/api/events/:eventid/budgetupdate' */
module.exports.eventsBudgetUpdate = function (req, res ) {
    if (!req.params.eventid) {
        sendJSONresponse(res, 404, {
            'message': 'Evento não encontrado, eventid é obrigatório'
        });
        return;
    }
    getUser(req, res, function(req, res, user) {
        EventModel
            .findById(req.params.eventid)
            .where({ user_id: user._id })
            .select('-venues -episodes -tasks -vendors -customers -messages -notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONResponse(res, 404, {
                            'message': 'eventid not found'
                        });
                        return;
                    } else if (err) {
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    event.budget = req.body.budget;
                    event.save(function (err, event) {
                        if (err) {
                            console.log(err);
                            sendJSONResponse(res, 400, err);
                        } else {
                            sendJSONResponse(res, 200, event);
                        }
                    });
                }
            );
    });
};

/** path: '/api/events/:eventid/archive' */
module.exports.eventsArchive = function (req, res ) {
    /** params: deve ter o parâmetro archive indicando 'true' ou 'false'. */
    if (!req.params.eventid) {
        sendJSONResponse(res, 404, { 'message': 'Evento não encontrado, eventid e obrigatório.' });
        return;
    } else if ( typeof(req.body.archive) !== 'boolean') {
        sendJSONResponse(res, 404, { 'message': 'parâmetro archive é obrigatório.' });
        return;
    }
    getUser(req, res, function(req, res, user) {
        EventModel
            .findById(req.params.eventid)
            .where({ user_id: user._id })
            .select('-venues -episodes -tasks -vendors -customers -messages -notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONResponse(res, 404, {
                            'message': 'eventid not found'
                        });
                        return;
                    } else if (err) {
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    event.archived = req.body.archive;
                    event.save(function (err, event) {
                        if (err) {
                            console.log(err);
                            sendJSONResponse(res, 400, err);
                        } else {
                            sendJSONResponse(res, 200, event);
                        }
                    });
                }
            );
    });
};

/** path: '/api/events/:eventid' */
module.exports.eventsDeleteOne = function(req, res) {
    var eventId = req.params.eventid;
    if (!eventId) {
        sendJSONResponse(res, 404, {
            'message': 'Não encontrado, eventid é obrigatório'
        });
        return;
    }
    getUser(req, res, function(req, res, user) {
        EventModel
            .findByIdAndRemove(eventId)
            .where({ user_id: user._id })
            .exec(
                function (err) {
                    if (err) {
                        console.log(err);
                        sendJSONResponse(res, 400, err);
                        return;
                    }
                    console.log('Event id ' + eventId + ' deleted.');
                    sendJSONResponse(res, 204, null);
                });
    });
};
