let mongoose = require('mongoose'),
    EventModel = mongoose.model('Event'),
    UserModel = mongoose.model('User'),
    sendGridEmail = require('../tools/sendGridEmail'),
    fs = require('fs'),
    sendJSONresponse = function(res, status, content) {
        res.status(status);
        res.json(content);
    };

/**
 * Generates general log for the Application
 * @param {String} req.body.text
 * */
module.exports.appLog = function (req) {
    console.log( req.body.text );
};

/**
 * Returns the URI of the Node Server
 * @return {String} URI in use
 * */
module.exports.serverUrl = function () {
    return process.env.NODE_ENV === 'production'  ? 'http://app.easyplanner.com.br/#' : 'http://localhost:3000/#'; // TODO: configurar no servidor a string com URL?
};


/**
 * Sends reminders for the tasks of the next 2 days
 * */
function getEvents(callback) {
    // Pega horário no time zone do Brasil - TODO: configurar para novos regiões
    let moment = require('moment-timezone');
    let startDate = moment.tz(new Date(), 'America/Sao_Paulo');
    let endDate   = moment.tz(new Date(startDate), 'America/Sao_Paulo');
    // Início do dia
    startDate.second(0);
    startDate.minute(0);
    startDate.hour(0);
    // Final do segundo a partir de hoje
    endDate.hour(23);
    endDate.minute(59);
    endDate.second(59);
    endDate.date(startDate.date() +2);

    EventModel
        .find({tasks: { $elemMatch: { $and: [
                        { dueDate: { $gte: startDate }},
                        { dueDate: { $lte: endDate }}
                    ]} }
        })
        .exec( function (err, events) {
            if (!events) {
                console.info('sem evento');
            } else if (err) {
                console.error(err);
            }
            callback(events, startDate, endDate);
        });
}

module.exports.getTasksRemainders = function () {
    getEvents(function (events, startDate, endDate) {
        events.forEach(function (event) {
            let eventTasks = {};
            eventTasks.mainCustomers = event.mainCustomers;
            eventTasks.type = event.type;
            UserModel
                .find({ _id: event.user_id })
                .exec( function (err, user) {
                    if(err) {
                        console.error(err);
                    }
                    if (!user) {
                        console.error('n achou user');
                    } else {
                        eventTasks.user = user;
                        eventTasks.tasks = [];
                        event.tasks.forEach(function (task) {
                            if (task.dueDate > startDate && task.dueDate < endDate) {
                                eventTasks.tasks.push(task);
                            }
                        });
                        sendEventEmail(eventTasks);
                    }
                });
        })
    })
};

let sendEventEmail = function (eventTasks) {
    let moment = require('moment-timezone');
    let subject = 'Lembrete das próximas compromissos do '+ eventTasks.type +' de ' + eventTasks.mainCustomers;
    let mailTo = eventTasks.user[0].email;
    let mailFrom = 'easyPlanner <suporte@easyplanner.com.br>';
    let mailText = 'Olá,\n' +
        'Este é um email automático para lembrá-lo do(s) compromisso(s):\n' +
        '\n';

    // Tasks
    eventTasks.tasks.forEach(function (task) {
        let data = moment( new Date(task.dueDate)).format('DD/MM/YYYY');
        let hora = moment( new Date(task.dueDate)).format('HH:mm');
        mailText += task.name.toUpperCase() + ' - data: ' + data + ' ' + hora + '\n';
        mailText += '\n';
    });

    mailText += '\n';
    mailText += 'Caso tenha dúvidas ou queria entrar em contato, simplesmente responda à este email\n';
    mailText += '\n';
    mailText += 'Obrigado,' + '\r\n';
    mailText += '\n';
    mailText += eventTasks.user[0].company + '\r\n';

    Email.sendEmail(subject, mailTo, mailFrom, mailText)
        .then(function () {
            let now = moment().format('DD/MM/YYY HH:mm:ss');
            console.log( now + ' - Email enviado com sucesso para: ' + JSON.stringify(mailTo) );
        })
        .catch(function (err) {
            console.err(err)
        })
};

/**
 * Service to send general e-mail in the Application
 * @path  '/api/sendmail'
 * @param req.body.subject  {String} Subject
 * @param req.body.mailTo   {String} Receipt(s)
 * @param req.body.mailFrom {String} Who is sending
 * @param req.body.mailText {String} E-mail text
 */
module.exports.sendGeneralEmail = function (req, res) {
    let subject  = req.body.subject,
        mailTo   = req.body.mailTo,
        mailBcc  = req.body.mailBcc,
        mailFrom = req.body.mailFrom,
        mailText = req.body.mailText,
        html2Pdf = req.body.html2Pdf,
        event    = req.body.event,
        htmlDone,
        cssMain, cssBootstrap, cssUi, // CSS files.

        moment = require('moment-timezone'),

        /** TODO: posteriormente separar para um função a conversão do PDF */
        pdf = require('html-pdf'),
        pdfOptions = {
            'type'   : 'pdf',
            'format' : 'Letter',
            'border' : {            // default is 0, units: mm, cm, in, px
                'top'   : '1cm',
                'right' : '1.5cm',
                'bottom': '0.2cm',
                'left'  : '1.5cm'
            },
            "header": {
                "height": "20mm",
                "contents": '<div style="text-align: left; font-size: 15px;">Evento: ' + event + ' </div>'
            },
            "footer": {
                "height": "28mm",
                "contents": {
                    default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>' // fallback value
                }
            },
        },
        pdfBuffer;

    let sendViaSendGrid = function (pdfBuffer) {
        sendGridEmail
            .sendEmail(subject, mailFrom, mailTo, mailBcc, mailText, pdfBuffer)
            .then(function () {
                let now = moment().format('DD/MM/YYY HH:mm:ss');
                console.log( now + ' - Email enviado com sucesso para: ' + JSON.stringify(mailTo) + JSON.stringify(mailBcc) );
                sendJSONresponse(res, 204, 'Email enviado com sucesso.');
            })
            .catch(function (err) {
                let now = moment().format('DD/MM/YYY HH:mm:ss');
                console.error('erro');
                console.error(now + ' - Erro ao enviar e-mail: ' + err);
                sendJSONresponse(res, 500, 'Erro ao enviar e-mail.');
            })
    };


    /**
     * Montar o header do HTML aqui para incluir o CSS
     */
    cssMain      = fs.readFileSync('app_api/controllers/styles/main.css', 'utf8');
    cssBootstrap = fs.readFileSync('app_api/controllers/styles/bootstrap.css', 'utf8');
    cssUi        = fs.readFileSync('app_api/controllers/styles/ui.css', 'utf8');

    htmlDone =
        '<html>' +
        '<head>' +
        '<style>'
        + cssMain + cssBootstrap + cssUi + // junta os CSS em uma string só.
        '</style>' +
        '</head>' +
        '<body>'
        + html2Pdf +
        '</body>' +
        '</html>';

    /**
     * Converte o PDF aqui antes de mandar para qualquer cliente.
     */
    if (html2Pdf) {
        pdf.create(htmlDone, pdfOptions).toBuffer(function (err, buffer) {
            pdfBuffer = new Buffer.from(buffer);
            sendViaSendGrid(pdfBuffer);
        });
    } else {
        sendViaSendGrid();
    }

    /**
     * Cria arquivo para teste do PDF
     */
    // pdf.create(htmlDone, pdfOptions).toFile('pdfTest.pdf', function(err, res) {
    //     if (err) return console.log(err);
    //     console.log(res); // { filename: '/app/businesscard.pdf' }
    // });

};
