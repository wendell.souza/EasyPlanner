var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        "message": "Usuário não encontrado."
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req,res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            "message": "Usuário não encontrado."
        });
    }
};

/* POST a new episode, but must provided with a eventid*/
/* /events/:eventid/episodes */
module.exports.episodesCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('episodes')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddepisodes(req, res, event);
                        }
                    }
                )
        });
    } else {
        sendJSONresponse(res, 404, {
            "message":"Não encontrado, eventid obrigatório."
        })
    }
};

var doAddepisodes = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, "eventid não encontrado.");
    } else {
        event.episodes.push({
            name:  req.body.name,
            hour:  req.body.hour,
            notes: req.body.notes
        });
        event.save(function(err, event){
            var thisEpisode;
            if (err) {
                sendJSONresponse(res, 404, err);
            } else {
                thisEpisode = event.episodes[event.episodes.length -1];
                sendJSONresponse(res, 201, thisEpisode);
            }
        });
    }
};


/** path: '/api/events/:eventid/episodes/:episodeid' */
module.exports.episodesUpdateOne = function (req, res ) {
    var thisEpisode;
    if (!req.params.eventid || !req.params.episodeid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e episodeid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('episodes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.episodes && event.episodes.length > 0) {
                        thisEpisode = event.episodes.id(req.params.episodeid);
                        if (!thisEpisode) {
                            sendJSONresponse(res, 404, {
                                "message": "episodeid not found"
                            });
                        } else {
                            thisEpisode.name  = req.body.name;
                            thisEpisode.hour  = req.body.hour;
                            thisEpisode.notes = req.body.notes;
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisEpisode);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum episode para atualizar"
                        });
                    }
                }
            );
    });
};

/** path: app.delete('/api/events/:eventid/episodes/:episodeid' */
module.exports.episodesDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.episodeid) {
        sendJSONresponse(res, 404, {
            "message": "Não encontrado, eventid e episodeid são obrigatórios"
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('episodes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            "message": "eventid não encontrado"
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.episodes && event.episodes.length > 0) {
                        if (!event.episodes.id(req.params.episodeid)) {
                            sendJSONresponse(res, 404, {
                                "message": "episodeid not found"
                            });
                        } else {
                            event.episodes.id(req.params.episodeid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            "message": "Nenhum episode para excluir"
                        });
                    }
                }
            );
    });
};
