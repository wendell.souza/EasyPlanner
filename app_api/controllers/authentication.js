var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Event = mongoose.model('Event');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.register =  function(req, res) {
    if (!req.body.name || !req.body.email || !req.body.password) {
        sendJSONresponse(res, 400, {
            "message" : "Todos os campos são obrigatórios: nome, email e senha."
        });
        return;
    }

    var user = new User();
    user.name       = req.body.name;
    user.email      = req.body.email;
    user.company    = req.body.company;
    user.isCustomer = req.body.isCustomer;
    user.active     = true; // TODO: Implementar ativação de conta do usuário

    /** Expects the password in bas64, so needs to decodify */
    req.body.password = Buffer.from(req.body.password, 'base64').toString('ascii');
    user.setPassword(req.body.password);

    user.save( function(err, user) {
        if (err) {
            console.log(err);
            sendJSONresponse(res, 404, err);
        } else {
            /** Retorna token pra logar o  usuário na aplicação */
            var token;
            if(err) {
                sendJSONresponse(res, 404, err);
                return;
            }
            if(user) {
                token = user.generateJwt();
                sendJSONresponse(res, 200, {"token" : token} );
            } else {
                sendJSONresponse(res, 401, info);
            }
        }
    });
};

module.exports.login = function (req, res) {
    if(!req.body.email || !req.body.password) {
        sendJSONresponse(res, 400, {
            "message": "Todos os campos são obrigatórios."
        });
        return;
    }
    /** Expects the password in bas64, so needs to decodify */
    req.body.password = Buffer.from(req.body.password, 'base64').toString('ascii');
    passport.authenticate('local', function(err, user, info) {
        var token;
        if(err) {
            sendJSONresponse(res, 404, err);
            return;
        }
        if(user) {
            token = user.generateJwt();
            sendJSONresponse(res, 200, {"token" : token} );
        } else {
            sendJSONresponse(res, 401, info);
        }
    }) (req, res);
};

module.exports.update = function (req, res) {
    if(!req.payload || !req.payload.email) {
        sendJSONresponse(res, 404, {"message" : "Usuário não encontrado."});
        return;
    }
    User
        .findOne({ _id: req.payload._id})
        .exec( function(err, user) {
            if (!user) {
                sendJSONresponse(res, 404, {"message" : "Usuário não encontrado."});
                return;
            } else if (err) {
                sendJSONresponse(res, 400, err);
                return;
            }
            user.name    = req.body.name;
            user.company = req.body.company;
            if (req.body.password) {
                user.setPassword(req.body.password);
            }
            user.save(function(err, user) {
                var token;
                if (err) {
                    console.log(err);
                    sendJSONresponse(res, 400, err);
                } else {
                    token = user.generateJwt();
                    sendJSONresponse(res, 200, {"token" : token});
                }
            })
        })
};